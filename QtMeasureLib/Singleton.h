﻿#ifndef SINGLETON_H
#define SINGLETON_H

namespace PerformanceHelper {

template <class T>
class Singleton : public T
{
public:

    static T& instance()
    {
        static T sInstance;
        return sInstance;
    }
};
}

#endif // SINGLETON_H
