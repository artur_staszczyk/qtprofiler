﻿#pragma once

#include <memory>

#include "HighPerfTimer.h"
#include "Storage/OutputStorage.h"

// MeasurementBuffer saved buffer layout:

// HEADER
// uint64 - header + file version
// uint64 - size in bytes of measured data

// MEASURED DATA
// double      double       uint32      uint32
// start_time  end_time     thread_id   index_to_string_list (function name)
// ...

// STRING LIST WITH FUNCTION NAMES
// uint64 - string count

// uint8 - string_length
// unsigned char x string_length - string
// ...

#include "MeasurementData.h"

namespace PerformanceHelper {
    class ConcurrentQueue;
}

namespace PerformanceHelper
{

#define ENTRY_SIZE_IN_BYTES (3 * sizeof(uint64_t))
#define FRAME_END_SIZE_IN_BYTES (sizeof(uint64_t))

#define DEFAULT_SIZE_IN_BYTES (1024 * 1024 * 100)
#define DEFAULT_MAX_FUNCTIONS 1024

class MeasurementBuffer
{
public:
    static uint64_t HEADER_VERSION;
    static uint64_t FRAME_END_MARKER;

public:

    MeasurementBuffer();
    ~MeasurementBuffer();

    void            setBufferSize(uint32_t bufferSizeInBytes);
    void            setMaxFunctionNames(uint32_t maxFunctionNames);
    void            setStorage(OutputStorage* storage);

    void            queueMeasurement(const MeasurementData& measurement);
    void            queueFrameEnd();

    // save and processSavedMeasurement MUST! be called from the same thread
    void            processSavedMeasurement();

    // save and processSavedMeasurement MUST! be called from the same thread
    void            save();

    const uint8_t * getMeasurementBuffer() const { return mMeasurementBuffer.get(); }
    const char**    getFunctionBuffer() const { return mFunctionNamesBuffer.get(); }

    uint32_t        sizeInBytesOfMeasuredData();

private:
    uint16_t        indexForFirstEmpty();
    uint16_t        nameIndexOrFirstEmpty(const char* name);

    void            saveMeasurement(const MeasurementData& measurement);
    void            saveFrameEnd();

    void            deleteFunctionNames();
    void            clearQueue();
    void            checkThread();

    void            printDebugInfo();

private:

    std::unique_ptr<OutputStorage>  mStorage;
    uint32_t                        mBufferSizeInBytes;
    uint16_t                        mMaxFunctionNames;

    std::unique_ptr<uint8_t[]>      mMeasurementBuffer;
    uint64_t*                       mBufferPointer;

    std::unique_ptr<const char*[]>  mFunctionNamesBuffer;

    std::size_t                     mLastThreadId;
    std::unique_ptr<PerformanceHelper::ConcurrentQueue> mLocklessQueue;
    //moodycamel::ConcurrentQueue<MeasurementData>& mLocklessQueue;

private:
    MeasurementBuffer(MeasurementBuffer&);
};

}
