﻿#pragma once

#include <cstdint>

namespace PerformanceHelper
{

//----------------------------------------------------------------
class HighPerfTimer
{

public:

    HighPerfTimer();
    double millisecondsElapsed() const;

private:
    void start();

private:
    double mFrequency;
    int64_t mStartTime;
};

//----------------------------------------------------------------
template <class TimerImplementation>
double  GetMilliseconds(const TimerImplementation& timer)
{
    return timer.millisecondsElapsed();
}

}
