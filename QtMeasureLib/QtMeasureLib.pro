ś#-------------------------------------------------
#
# Project created by QtCreator 2015-07-24T18:30:38
#
#-------------------------------------------------
include(../include.pri)

CONFIG(debug, debug|release): LIB_NAME = measureLibd
CONFIG(release, debug|release): LIB_NAME = measureLib

QT       -= core gui

TEMPLATE = lib
CONFIG += staticlib
TARGET = $$LIB_NAME

SOURCES += \
    $$PWD/MeasurementBuffer.cpp \
    Storage/FileOutputStorage.cpp \
    Storage/NullOutputStorage.cpp \
    ConcurrentQueue.cpp \
    Measurement.cpp

win32:SOURCES += $$PWD/HighPerfTimerWin.cpp
else:macx:SOURCES += $$PWD/HighPerfTimerMac.cpp

HEADERS += \
    $$PWD/Measurement.h \
    $$PWD/MeasurementBuffer.h \
    $$PWD/PerformanceHelper.h \
    $$PWD/HighPerfTimer.h \
    Storage/OutputStorage.h \
    Singleton.h \
    Storage/FileOutputStorage.h \
    Storage/NullOutputStorage.h \
    MeasurementData.h \
    ConcurrentQueue.h

macx|win32: {
    target.path = $$PWD/../libs-qt
    INSTALLS += target

    COPY_HEADERS += $$relative_path($$PWD/*.h)
    COPY_HEADERS += $$relative_path($$PWD/Storage/*.h)

    HEADER_INSTALL_PREFIX = $$PWD/../include/MeasureLib
    for(header, COPY_HEADERS) {
      path = $${HEADER_INSTALL_PREFIX}/$${dirname(header)}
      pathDir = $${dirname(header)}

      eval(headers_$${pathDir}.files += $$header)
      eval(headers_$${pathDir}.path = $$path)
      eval(INSTALLS *= headers_$${pathDir})
    }
}
