﻿#include "HighPerfTimer.h"

#include <mach/mach.h>
#include <mach/mach_time.h>

using namespace PerformanceHelper;

static mach_timebase_info_data_t    sTimebaseInfo;

HighPerfTimer::HighPerfTimer()
{
    start();
}

void HighPerfTimer::start()
{
    mach_timebase_info(&sTimebaseInfo);
    mFrequency = (double)sTimebaseInfo.numer / (double)sTimebaseInfo.denom;

    mStartTime = mach_absolute_time();
}

double HighPerfTimer::millisecondsElapsed() const
{
    int64_t endTime = mach_absolute_time();
    return int64_t (endTime - mStartTime) * mFrequency / 1000000;
}
