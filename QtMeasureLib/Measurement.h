﻿#pragma once

#include "HighPerfTimer.h"
#include "MeasurementData.h"
#include "MeasurementBuffer.h"

namespace PerformanceHelper
{

#ifdef ENABLE_PROFILING
#define MEASURE_SCOPE_START(name) { \
    PerformanceHelper::Measurement<PerformanceHelper::MeasurementBuffer, PerformanceHelper::HighPerfTimer> \
    measurement( \
        name, \
        PerformanceHelper::Singleton<PerformanceHelper::MeasurementBuffer>::instance(), \
        PerformanceHelper::Singleton<PerformanceHelper::HighPerfTimer>::instance());

#define MEASURE_SCOPE_END() }

#define FRAME_END() PerformanceHelper::Singleton<PerformanceHelper::MeasurementBuffer>::instance().queueFrameEnd();
#else 
#define MEASURE_SCOPE_START(name) { (void)(name);
#define MEASURE_SCOPE_END() }
#define FRAME_END() 
#endif

template <class MeasurementBufferDef, class TimerDef>
class Measurement
{
public:
    Measurement(const char* measurementName, MeasurementBufferDef& measurementBuffer, const TimerDef& timer);
    ~Measurement();

    MeasurementData Data;

private:

    MeasurementBufferDef& mMeasurementBuffer;
    const TimerDef& mTimer;
};

std::size_t GetThreadCurrentID();

template <class MeasurementBufferDef, class TimerDef>
Measurement<MeasurementBufferDef, TimerDef>::Measurement(const char* measurementName, MeasurementBufferDef& measurementBuffer, const TimerDef& timer)
    : mMeasurementBuffer(measurementBuffer)
    , mTimer(timer)
{
    Data.EndTime = 0.0;
    Data.Name = measurementName;
    Data.StartTime = GetMilliseconds<TimerDef>(timer);
    Data.ThreadId = GetThreadCurrentID();
}

template <class MeasurementBufferDef, class TimerDef>
Measurement<MeasurementBufferDef, TimerDef>::~Measurement()
{
    Data.EndTime = GetMilliseconds<TimerDef>(mTimer);
    mMeasurementBuffer.queueMeasurement(Data);
}

}
