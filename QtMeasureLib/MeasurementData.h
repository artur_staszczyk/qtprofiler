﻿#ifndef MEASUREMENTDATA_H
#define MEASUREMENTDATA_H

#include <string>

namespace PerformanceHelper
{

    struct MeasurementData
    {
        double StartTime;
        double EndTime;
        const char* Name;
        std::size_t ThreadId;
    };

}

#endif // MEASUREMENTDATA_H
