#include "Measurement.h"

#include <thread>

namespace PerformanceHelper
{

std::size_t GetThreadCurrentID()
{
    return std::hash<std::thread::id>()(std::this_thread::get_id());
}

}
