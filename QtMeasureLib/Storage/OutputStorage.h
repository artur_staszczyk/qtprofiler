﻿#ifndef OUTPUTSTORAGE_H
#define OUTPUTSTORAGE_H

#include <stdint.h>

namespace PerformanceHelper {

class OutputStorage
{
public:
    virtual ~OutputStorage(){}

    virtual void store(const uint8_t* data, uint32_t size) = 0;
    virtual void load(uint8_t** data, uint32_t& size) = 0;
};

}

#endif // OUTPUTSTORAGE_H
