﻿#include "FileOutputStorage.h"

using namespace PerformanceHelper;

FileOutputStorage::FileOutputStorage(const std::string& fileName)
    : mFileName(fileName)
{

}

void FileOutputStorage::store(const uint8_t* data, uint32_t size)
{
    FILE* filePointer;
    filePointer = fopen(mFileName.c_str(), "wb");
    fwrite(data, sizeof(uint8_t), size, filePointer);
    fclose(filePointer);
}

void FileOutputStorage::load(uint8_t **data, uint32_t& size)
{
    FILE* filePointer;
    filePointer = fopen(mFileName.c_str(), "rb");

    fseek(filePointer , 0 , SEEK_END);
    size = ftell(filePointer);
    rewind(filePointer);

    *data = new uint8_t[size];
    fread(data, size, sizeof(char), filePointer);

    fclose(filePointer);
}
