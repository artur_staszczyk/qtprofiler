﻿#ifndef FILEOUTPUTSTORAGE_H
#define FILEOUTPUTSTORAGE_H

#include <string>
#include "OutputStorage.h"

namespace PerformanceHelper {

class FileOutputStorage : public OutputStorage
{
public:
    FileOutputStorage(const std::string& fileName);

    virtual void store(const uint8_t* data, uint32_t size) override;
    virtual void load(uint8_t** data, uint32_t& size) override;

private:
    std::string mFileName;
};

}

#endif // FILEOUTPUTSTORAGE_H
