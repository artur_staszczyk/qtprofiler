﻿#ifndef NULLOUTPUTSTORAGE_H
#define NULLOUTPUTSTORAGE_H

#include "OutputStorage.h"

namespace PerformanceHelper {

class NullOutputStorage : public OutputStorage
{
public:
    NullOutputStorage();

    void store(const uint8_t* data, uint32_t size) override
    {
        (void)data;
        (void)size;
    }

    void load(uint8_t** data, uint32_t& size) override
    {
        (void)data;
        (void)size;
    }
};

}

#endif // NULLOUTPUTSTORAGE_H
