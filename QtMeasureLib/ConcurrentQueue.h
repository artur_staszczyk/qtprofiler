#ifndef COCURENTQUEUE_H
#define COCURENTQUEUE_H

#include <cstdint>

#include "MeasurementData.h"
#include "concurrentqueue/concurrentqueue.h"

namespace PerformanceHelper {

    class ConcurrentQueue : public moodycamel::ConcurrentQueue<MeasurementData>
    {
    public:
        ConcurrentQueue(size_t initialSize)
            : moodycamel::ConcurrentQueue<MeasurementData>(initialSize)
        {}
    };

}
#endif // COCURENTQUEUE_H
