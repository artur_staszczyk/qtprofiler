﻿#define _CRT_SECURE_NO_WARNINGS

#include "MeasurementBuffer.h"

#include <iomanip>
#include <iostream>

#include "ConcurrentQueue.h"
#include "Measurement.h"
#include "Storage/NullOutputStorage.h"
#include "simplebinstream/TestBinStream/SimpleBinStream.h"
#include "concurrentqueue/concurrentqueue.h"

using namespace PerformanceHelper;

uint64_t MeasurementBuffer::HEADER_VERSION = 0xEEEEEEEE00000002;
uint64_t MeasurementBuffer::FRAME_END_MARKER = 0x00000000FFFFFFFF;

MeasurementBuffer::MeasurementBuffer(MeasurementBuffer &)
    //: mLocklessQueue(moodycamel::ConcurrentQueue<MeasurementData>())
    : mLocklessQueue(new ConcurrentQueue(sizeof(MeasurementData)))
{}

MeasurementBuffer::MeasurementBuffer()
    : mStorage(new NullOutputStorage())
    , mBufferPointer(nullptr)
    , mLastThreadId(0)
    , mLocklessQueue(new ConcurrentQueue(64 * sizeof(MeasurementData)))
    //, mLocklessQueue(moodycamel::ConcurrentQueue<MeasurementData>(64 * sizeof(MeasurementData)))
{
    setBufferSize(DEFAULT_SIZE_IN_BYTES);
    setMaxFunctionNames(DEFAULT_MAX_FUNCTIONS);
}

MeasurementBuffer::~MeasurementBuffer()
{
    processSavedMeasurement();
    save();
}

void MeasurementBuffer::setBufferSize(uint32_t bufferSizeInBytes)
{
    mBufferSizeInBytes = bufferSizeInBytes;

    mMeasurementBuffer.reset(new uint8_t[mBufferSizeInBytes]);
    memset(mMeasurementBuffer.get(), 0, sizeof(uint8_t) * mBufferSizeInBytes);
    mBufferPointer = reinterpret_cast<uint64_t*>(mMeasurementBuffer.get());

    clearQueue();
}

void MeasurementBuffer::setMaxFunctionNames(uint32_t maxFunctionNames)
{
    deleteFunctionNames();

    mMaxFunctionNames = maxFunctionNames;
    mFunctionNamesBuffer.reset(new const char*[mMaxFunctionNames]);
    memset(mFunctionNamesBuffer.get(), 0, sizeof(char*) * mMaxFunctionNames);

    clearQueue();
}

void MeasurementBuffer::deleteFunctionNames()
{
    if(mFunctionNamesBuffer)
    {
        for(auto cnt = 0u; cnt < mMaxFunctionNames; ++cnt)
        {
            if(mFunctionNamesBuffer[cnt])
                std::free(const_cast<char*>(mFunctionNamesBuffer[cnt]));
        }
    }
}

void MeasurementBuffer::clearQueue()
{
    MeasurementData item;
    while(mLocklessQueue->try_dequeue(item));
}

void MeasurementBuffer::setStorage(OutputStorage* storage)
{
    mStorage.reset(storage);
    setBufferSize(mBufferSizeInBytes);
    setMaxFunctionNames(mMaxFunctionNames);
}

void MeasurementBuffer::queueMeasurement(const MeasurementData& measurement)
{
    mLocklessQueue->enqueue(measurement);
}

uint16_t MeasurementBuffer::nameIndexOrFirstEmpty(const char* name)
{
    int index = mMaxFunctionNames;
    bool found = false;

    for (uint16_t i = 0; i < mMaxFunctionNames; ++i)
    {
        if (mFunctionNamesBuffer[i] == nullptr)
        {
            index = i;
            break;
        }

        if (!strcmp(mFunctionNamesBuffer[i], name))
        {
            index = i;
            found = true;
            break;
        }
    }

    return index;
}

void MeasurementBuffer::queueFrameEnd()
{
    MeasurementData frameEnd;
    frameEnd.ThreadId = reinterpret_cast<std::size_t&>(FRAME_END_MARKER);
    mLocklessQueue->enqueue(frameEnd);
}

void MeasurementBuffer::processSavedMeasurement()
{
    checkThread();

    MeasurementData measurementData;
    while(mLocklessQueue->try_dequeue(measurementData))
    {
        if(measurementData.ThreadId == reinterpret_cast<std::size_t&>(FRAME_END_MARKER))
            saveFrameEnd();
        else
            saveMeasurement(measurementData);
    }
}

void MeasurementBuffer::saveMeasurement(const MeasurementData &measurement)
{
    uint32_t nameIndex = nameIndexOrFirstEmpty(measurement.Name);

    if (nameIndex >= mMaxFunctionNames)
        return;

    if (sizeInBytesOfMeasuredData() + ENTRY_SIZE_IN_BYTES > mBufferSizeInBytes)
        return;

    *mBufferPointer = reinterpret_cast<const uint64_t&>(measurement.StartTime);
    mBufferPointer++;

    *mBufferPointer = reinterpret_cast<const uint64_t&>(measurement.EndTime);
    mBufferPointer++;

    uint32_t* tempBufferPointer = reinterpret_cast<uint32_t*>(mBufferPointer);
    *tempBufferPointer = reinterpret_cast<const uint32_t&>(measurement.ThreadId);
    tempBufferPointer++;

    if (mFunctionNamesBuffer[nameIndex] == nullptr)
    {
#ifdef _WIN32
        mFunctionNamesBuffer[nameIndex] = _strdup(measurement.Name);
#else
        mFunctionNamesBuffer[nameIndex] = strdup(measurement.Name);
#endif
    }

   *tempBufferPointer = reinterpret_cast<const uint32_t&>(nameIndex);

    mBufferPointer++;
}

void MeasurementBuffer::saveFrameEnd()
{
    if (sizeInBytesOfMeasuredData() + 8 >= mBufferSizeInBytes)
        return;

    *mBufferPointer = FRAME_END_MARKER;
    mBufferPointer++;
}

uint32_t MeasurementBuffer::sizeInBytesOfMeasuredData()
{
    return reinterpret_cast<uint8_t*>(mBufferPointer) - mMeasurementBuffer.get();
}


void MeasurementBuffer::save()
{
    checkThread();

    simple::mem_ostream<std::true_type> ostream;
    ostream << HEADER_VERSION;
    ostream << static_cast<uint64_t>(sizeInBytesOfMeasuredData());
    ostream.write(reinterpret_cast<char*>(mMeasurementBuffer.get()), static_cast<size_t>(sizeInBytesOfMeasuredData()));

    auto namesCount = indexForFirstEmpty();
    ostream << static_cast<uint64_t>(namesCount);
    for (auto i = 0; i < namesCount; ++i)
    {
        auto strSize = strlen(mFunctionNamesBuffer[i]);
        ostream << static_cast<uint8_t>(strSize);
        ostream.write(mFunctionNamesBuffer[i], strSize);
    }

    auto internalVec = ostream.get_internal_vec();
    const uint8_t* data = reinterpret_cast<const uint8_t*>(internalVec.data());
    mStorage->store(data, internalVec.size());

#if defined _DEBUG_PROFILE
    printDebugInfo();
#endif
}

uint16_t MeasurementBuffer::indexForFirstEmpty()
{
    int index = mMaxFunctionNames;

    for (uint16_t i = 0; i < mMaxFunctionNames; ++i)
    {
        if (mFunctionNamesBuffer[i] == nullptr)
        {
            index = i;
            break;
        }
    }

    return index;
}

void MeasurementBuffer::printDebugInfo()
{
    std::cout.precision(17);
    std::cout << "File header version: " << std::hex << HEADER_VERSION << std::endl;
    std::cout << "File data length: " << sizeInBytesOfMeasuredData() << std::endl;

    uint64_t* buffer = reinterpret_cast<uint64_t*>(mMeasurementBuffer.get());
    for (uint64_t i = 0; i < sizeInBytesOfMeasuredData() / sizeof(uint64_t);)
    {
        uint64_t testVal = buffer[i];
        if (testVal != FRAME_END_MARKER)
        {
            uint64_t stringId = buffer[i + 2];
            std::cout << mFunctionNamesBuffer[stringId] <<
                " start time: " << std::fixed << reinterpret_cast<double&>(buffer[i]) <<
                " end time: " << std::fixed << reinterpret_cast<double&>(buffer[i + 1]) << std::endl;
            i += 3;
        }
        else
        {
            std::cout << "Frame end" << std::endl;
            i += 1;
        }
    }
}

void MeasurementBuffer::checkThread()
{
    if(mLastThreadId == 0)
    {
        mLastThreadId = std::hash<std::thread::id>()(std::this_thread::get_id());
    }
    else
    {
        assert(mLastThreadId == std::hash<std::thread::id>()(std::this_thread::get_id()) && "Function run from different thread");
    }
}
