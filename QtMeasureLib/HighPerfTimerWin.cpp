#include "HighPerfTimer.h"

#include <Windows.h>
#include <stdio.h>

using namespace PerformanceHelper;

HighPerfTimer::HighPerfTimer()
{
    start();
}

void HighPerfTimer::start()
{
    LARGE_INTEGER freq;
    auto initialized = (QueryPerformanceFrequency(&freq));
    if (initialized)
        mFrequency = double(freq.QuadPart) / 1000.0;

	LARGE_INTEGER counter;
	QueryPerformanceCounter(&counter);
	mStartTime = counter.QuadPart;
}

double HighPerfTimer::millisecondsElapsed() const
{
	LARGE_INTEGER counter;
	QueryPerformanceCounter(&counter);
	return double(counter.QuadPart - mStartTime) / mFrequency;
}

