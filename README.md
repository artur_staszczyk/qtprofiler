### Layout ###
This repo contains 2 libraries and one application:
- measureLib
- profilerLib
- profilerApp

### measureLib ###
To insure highest possible performance this calss should not have any virtual methods.
For that reason Google Mock library cannot use mocking by inheritance but mocking by templating.
Hence sometimes strange constructs in the code

Uses:
- c++ binstrem https://github.com/shaovoon/simplebinstream
- c++ lock free queue: https://github.com/cameron314/concurrentqueue

### profilerApp ###

Using icons from http://www.flaticon.com/packs/general-ui
TODO: Include licence info

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
