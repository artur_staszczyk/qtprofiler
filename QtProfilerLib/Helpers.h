﻿#ifndef HELPERS_H
#define HELPERS_H

#include <iostream>
#include <ctime>
#include <random>

namespace star_help {

inline std::string format(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    #ifndef _MSC_VER
        size_t size = std::snprintf(nullptr, 0, format, args) + 1; // Extra space for '\0'
        std::unique_ptr<char[]> buf(new char[size]);
        std::vsnprintf(buf.get(), size, format, args);
        return std::string(buf.get(), buf.get() + size - 1 ); // We don't want the '\0' inside
    #else
        int size = _vscprintf(format, args);
        std::string result(++size, 0);
        vsnprintf_s((char*)result.data(), size, _TRUNCATE, format, args);
        return result;
    #endif
    va_end(args);
}

class Random {
public:
    static void Initialize() {
        Generator.seed(time(0));
    }

    template <typename T>
    static T getInt(T min, T max) {
        return std::uniform_int_distribution<T>(min, max)(Generator);
    }

    template <typename T>
    static float getReal(T min, T max) {
        return std::uniform_real_distribution<T>(min, max)(Generator);
    }

    template <typename T>
    static T nextInt()
    {
        return getInt<T>(std::numeric_limits<T>::min(), std::numeric_limits<T>::max());
    }

    template <typename T>
    static T nextReal()
    {
        return getReal<T>(std::numeric_limits<T>::min(), std::numeric_limits<T>::max());
    }
private:
    static std::mt19937 Generator;
};

}

#endif // HELPERS_H
