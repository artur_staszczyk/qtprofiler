﻿#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include "precompiled.h"

class bad_version : public std::runtime_error
{
public:
    bad_version(const std::string& message)
        : std::runtime_error(message)
    {}
};

#endif // EXCEPTIONS_H
