﻿#ifndef PRECOMPILED_H
#define PRECOMPILED_H

#include <QSet>
#include <QVector>
#include <QPointF>
#include <QObject>
#include <QVariantMap>
#include <QDataStream>
#include <QtAlgorithms>

#include <QByteArray>
#include <QDataStream>

// STL
#include <tuple>
#include <sstream>

// ProfilerLib
#include "Exceptions.h"
#include "Helpers.h"

#endif // PRECOMPILED_H
