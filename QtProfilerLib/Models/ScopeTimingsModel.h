﻿#ifndef SCOPETIMINGSMODEL_H
#define SCOPETIMINGSMODEL_H

#include <QObject>
#include "AutoProperty.h"

class ScopeTimingsModel : public QObject
{
    Q_OBJECT
public:
    explicit ScopeTimingsModel(QObject *parent = 0);

    READONLY_PROPERTY(quint64, UniqueId, uniqueId)
    AUTO_PROPERTY(bool, IsGroup, isGroup)
    AUTO_PROPERTY(double, Start, start)
    AUTO_PROPERTY(double, Stop, stop)
    AUTO_PROPERTY(double, Average, average)
    AUTO_PROPERTY(quint32, Count, count)
    AUTO_PROPERTY(double, MaxDistance, maxDistance)
};

#endif // SCOPETIMINGSMODEL_H
