﻿#ifndef RAWFRAMEADAPTER_H
#define RAWFRAMEADAPTER_H

#include <queue>
#include <functional>
#include <QString>
#include <QPointF>
#include "IFrameDataModel.h"


class FrameDataModel : public IFrameDataModel
{
    Q_OBJECT

public:
    FrameDataModel(QObject* parent = nullptr);

    void setRawDataAdapter(IRawDataAdapter* rawDataAdapter) override;

    ThreadModel* findThreadNamed(const QString& name) const override { return findThreadNamed(aThreadsRaw, name); }
    std::tuple<QString, QString, ScopeTimingsModel*> findTimingWithId(quint64 timingId) const override;

private slots:
    void reactOnFrameChanged(quint32 frame);

private:
    void cacheFrame();

private:

    IRawDataAdapter* mRawDataAdapter;

    ThreadModel* findThreadNamed(const QList<ThreadModel*>& timingsModel, const QString& name) const;
    ThreadModel* appendThreadNamed(QList<ThreadModel*>& timingsModel, const QString& threadCaption);
};

#endif // RAWFRAMEADAPTER_H
