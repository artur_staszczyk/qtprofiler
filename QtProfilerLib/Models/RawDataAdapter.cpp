﻿#include "precompiled.h"

#include "Models/RawDataAdapter.h"
#include "FileLoading/FileDataConverter.h"

#include <QDataStream>

RawDataAdapter& RawDataAdapter::setRawMeasurementData(const QByteArray& rawByteArray)
{
    mRawMeasurementData = rawByteArray;
    return *this;
}

RawDataAdapter& RawDataAdapter::setPerFrameDataOffsets(const QVector<quint32>& dataOffsetForFrame)
{
    mFramesOffsets = dataOffsetForFrame;
    return *this;
}

RawDataAdapter& RawDataAdapter::setPerFrameScopesCount(const QVector<quint32>& measurementCountForFrame)
{
    mFramesMeasurementCount = measurementCountForFrame;
    return *this;
}

RawDataAdapter& RawDataAdapter::setPerFrameTimings(const QVector<double>& timings)
{
    mFramesTimings = timings;
    return *this;
}

RawDataAdapter& RawDataAdapter::setGlobalScopeNames(const QVector<QString>& scopes)
{
    mScopeNames = scopes;
    return *this;
}

RawDataAdapter& RawDataAdapter::setThreads(const QVector<quint32>& threads)
{
    mThreads = threads;
    return *this;
}

QString RawDataAdapter::getScopeNameForIndex(quint32 idx) const
{
    if(idx < static_cast<quint32>(mScopeNames.size()))
        return mScopeNames.at(idx);

    throw std::out_of_range("No such scope");
}

quint32 RawDataAdapter::getDataOffsetForFrame(quint32 frame) const
{
    if(frame < static_cast<quint32>(mFramesOffsets.size()))
        return mFramesOffsets.at(frame);

    throw std::out_of_range("No offset for such frame");
}

quint32 RawDataAdapter::getMeasurementCountForFrame(quint32 frame) const
{
    if(frame < static_cast<quint32>(mFramesMeasurementCount.size()))
        return mFramesMeasurementCount.at(frame);

    throw std::out_of_range("No scopes for such frame");
}

double RawDataAdapter::getFrameDuration(quint32 frame) const
{
    if(frame < static_cast<quint32>(mFramesTimings.size()))
        return mFramesTimings.at(frame);

    throw std::out_of_range("No timings for such frame");
}

double RawDataAdapter::maxFrameDuration() const
{
    return *std::max_element(mFramesTimings.begin(), mFramesTimings.end());
}

void RawDataAdapter::readFrameMeasurements(quint32 frameId, std::function< void (double, double, quint32, quint32)> lambda)
{
    auto scopesCount = getMeasurementCountForFrame(frameId - 1);
    auto dataOffset = getDataOffsetForFrame(frameId - 1);
    auto bytes = getRawMeasurementsData();

    QDataStream dataStream(bytes);
    dataStream.setByteOrder(QDataStream::LittleEndian);
    dataStream.skipRawData(dataOffset * sizeof(quint64));

    double startTime;
    double endTime;
    quint64 complexValue;

    quint32 scopesRead = 0;

    do
    {
        dataStream >> startTime;
        dataStream >> endTime;
        dataStream >> complexValue;

        lambda(startTime, endTime, FileDataConverter::threadIdFromComplexValue(complexValue), FileDataConverter::scopeIdFromComplexValue(complexValue));

        scopesRead++;
    } while(scopesRead < scopesCount);
}
