﻿#include "precompiled.h"

#include "FrameDataModel.h"
#include "Models/IRawDataAdapter.h"

#include <QDebug>

FrameDataModel::FrameDataModel(QObject* parent)
    : IFrameDataModel(parent)
    , mRawDataAdapter(nullptr)
{
    connect(this, SIGNAL(activeFrameChanged(quint32)), this, SLOT(reactOnFrameChanged(quint32)));
}

void FrameDataModel::setRawDataAdapter(IRawDataAdapter *rawDataAdapter)
{
    mRawDataAdapter = rawDataAdapter;
}

void FrameDataModel::reactOnFrameChanged(quint32 frame)
{
    if(frame == 0)
        throw std::invalid_argument("Trying to set bad frame index.");

    if(mRawDataAdapter == nullptr || frame > mRawDataAdapter->getFramesCount())
        throw std::invalid_argument("Trying to set bad frame index.");

    cacheFrame();
}

void FrameDataModel::cacheFrame()
{
    QList<ThreadModel*> timingsModel;

    double minStartTime = 10e10;
    double maxEndTime = -10e10;

    mRawDataAdapter->readFrameMeasurements(getActiveFrame(), [&](double startTime, double endTime, quint32 threadId, quint32 scopeIdx)
    {
        if(startTime < minStartTime)
            minStartTime = startTime;
        if(endTime > maxEndTime)
            maxEndTime = endTime;

        ThreadModel* threadScope = nullptr;
        auto threadCaption = QString("Thread 0x%1").arg(QString("%1").arg(threadId, 0, 16).toUpper());
        try
        {
            threadScope = findThreadNamed(timingsModel, threadCaption);
        }
        catch(std::invalid_argument)
        {
            threadScope = appendThreadNamed(timingsModel, threadCaption);
        }

        auto scopeName = mRawDataAdapter->getScopeNameForIndex(scopeIdx);
        ScopeModel* scopeData = nullptr;
        try
        {
            scopeData = threadScope->findScopeNamed(scopeName);
        }
        catch(std::invalid_argument)
        {
            scopeData = threadScope->appendScopeNamed(scopeName);
        }

        auto scopeTiming = new ScopeTimingsModel(scopeData);
        scopeTiming->setStart(startTime);
        scopeTiming->setStop(endTime);
        scopeData->addTimings(scopeTiming);
    });

    setFrameStartTime(minStartTime);
    setFrameEndTime(maxEndTime);

    swapThreads(timingsModel);

    for(auto element : timingsModel)
        delete element;
}

ThreadModel* FrameDataModel::findThreadNamed(const QList<ThreadModel*>& timingsModel, const QString& name) const
{
    for(ThreadModel* thread : timingsModel)
    {
        if(thread->getThreadId() == name)
        {
            return thread;
        }
    }

    throw std::invalid_argument("No such thread id");
}

ThreadModel* FrameDataModel::appendThreadNamed(QList<ThreadModel*>& timingsModel, const QString& threadCaption)
{
    auto threadScope = new ThreadModel(this);
    threadScope->setThreadId(threadCaption);
    timingsModel.append(threadScope);
    return threadScope;
}

std::tuple<QString, QString, ScopeTimingsModel*> FrameDataModel::findTimingWithId(quint64 timingId) const
{
    for(auto& thread : aThreadsRaw)
    {
        for(auto& scope : thread->getScopesRaw() )
        {
            for(auto& timing : scope->getTimingsRaw())
            {
                if(timing->getUniqueId() == timingId)
                {
                    return std::make_tuple(thread->getThreadId(), scope->getItemName(), timing);
                }
            }
        }
    }

    throw std::invalid_argument(star_help::format("Unable to find timngs with ID: %lld", timingId));
}
