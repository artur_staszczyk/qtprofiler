﻿#ifndef SCOPEDATAMODEL_H
#define SCOPEDATAMODEL_H

#include <QObject>

#include "AutoProperty.h"
#include "ScopeTimingsModel.h"

class ScopeModel : public QObject
{
    Q_OBJECT
public:
    explicit ScopeModel(QObject *parent = 0);

    AUTO_PROPERTY(QString, ItemName, itemName)
    LIST_PROPERTY(ScopeTimingsModel, Timings, timings)

public:
    void addTimings(double startTime, double endTime);

    ScopeTimingsModel* findTimings(quint64 timingId);
};

#endif // SCOPEDATAMODEL_H
