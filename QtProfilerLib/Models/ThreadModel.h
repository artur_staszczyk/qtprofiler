﻿#ifndef FRAMETIMINGSMODEL_H
#define FRAMETIMINGSMODEL_H

#include <QObject>
#include <QMetaType>

#include "AutoProperty.h"
#include "ScopeModel.h"

class ThreadModel : public QObject
{
    Q_OBJECT
public:
    explicit ThreadModel(QObject *parent = 0);

    AUTO_PROPERTY(QString, ThreadId, threadId)
    AUTO_PROPERTY(bool, Collapsed, collapsed)
    LIST_PROPERTY(ScopeModel, Scopes, scopes)

public:
    ScopeModel* findScopeNamed(const QString& scopeName);
    ScopeModel* appendScopeNamed(const QString& scopeName);

};

#endif // FRAMETIMINGSMODEL_H
