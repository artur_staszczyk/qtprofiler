﻿#include "ThreadModel.h"

ThreadModel::ThreadModel(QObject *parent)
    : QObject(parent)
{

}

ScopeModel* ThreadModel::findScopeNamed(const QString& scopeName)
{
    for(auto scope : aScopesRaw)
        if(scope->getItemName() == scopeName)
            return scope;

    throw std::invalid_argument("Invalid scope name");
}

ScopeModel* ThreadModel::appendScopeNamed(const QString& scopeName)
{
    auto scope = new ScopeModel(this);
    scope->setItemName(scopeName);
    addScopes(scope);
    return scope;
}
