﻿#include "precompiled.h"

#include "ScopeModel.h"

ScopeModel::ScopeModel(QObject *parent) : QObject(parent)
{

}

void ScopeModel::addTimings(double startTime, double endTime)
{
    auto timings = new ScopeTimingsModel(this);
    timings->setIsGroup(false);
    timings->setStart(startTime);
    timings->setStop(endTime);

    addTimings(timings);
}

ScopeTimingsModel* ScopeModel::findTimings(quint64 timingId)
{
    for(auto& timing : aTimingsRaw)
    {
        if(timing->getUniqueId() == timingId)
            return timing;
    }

    throw std::invalid_argument(star_help::format("Cannot fint ScopeTimingsModel with ID: %lld in scope %s.", timingId, aItemName.toStdString().c_str()));
}
