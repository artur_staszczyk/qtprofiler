﻿#ifndef SELECTIONMODEL_H
#define SELECTIONMODEL_H

#include <QObject>

#include "AutoProperty.h"
#include "ScopeTimingsModel.h"

class IFrameDataModel;

class SelectedScopeModel : public QObject
{
    Q_OBJECT

public:

    explicit SelectedScopeModel(IFrameDataModel& frameDataModel, QObject* parent = nullptr);

    AUTO_PROPERTY(quint64, SelectedTimingId, selectedTimingId)

    AUTO_PROPERTY(bool, AnyScopeSelected, anyScopeSelected)
    AUTO_PROPERTY(ScopeTimingsModel*, SelectedTiming, selectedTiming)

    AUTO_PROPERTY(bool, IsGroup, isGroup)
    AUTO_PROPERTY(quint32, CallCount, callCount)
    AUTO_PROPERTY(double, MaxDistance, maxDistance)
    AUTO_PROPERTY(double, AverageDuration, averageDuration)

    AUTO_PROPERTY(QString, ThreadId, threadId)
    AUTO_PROPERTY(QString, SelectedScopeName, selectedScopeName)
    AUTO_PROPERTY(double, Duration, duration)
    AUTO_PROPERTY(quint32, CallNumber, callNumber)

private slots:
    void reactOnSelectedTimingIdChanged(quint64 timingId);

private:
    void zero();

private:
    IFrameDataModel& mFrameDataModel;
};

#endif // SELECTIONMODEL_H
