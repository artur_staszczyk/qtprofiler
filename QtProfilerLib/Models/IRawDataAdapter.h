﻿#ifndef IRAWDATAADAPTER_H
#define IRAWDATAADAPTER_H

#include <functional>
#include <QObject>

class IRawDataAdapter : public QObject
{
    Q_OBJECT

public:

    IRawDataAdapter(QObject* parent = nullptr)
        : QObject(parent)
    {}

public:
    virtual quint32         getFramesCount() const = 0;

    virtual double          getFrameDuration(quint32 frame) const = 0;
    virtual quint32         getDataOffsetForFrame(quint32 frame) const = 0;
    virtual quint32         getMeasurementCountForFrame(quint32 frame) const = 0;

    virtual QByteArray      getRawMeasurementsData() const = 0;

    virtual quint32         getScopeNamesCount() const = 0;
    virtual QString         getScopeNameForIndex(quint32 idx) const = 0;

    virtual quint32         getThreadCount() const = 0;
    virtual quint32         getThreadName(quint32 threadIdx) const = 0;

    virtual double          maxFrameDuration() const = 0;
    virtual void            readFrameMeasurements(quint32 frameId,
                                                  std::function< void (double startTime, double endTime, quint32 threadId, quint32 scopeName)> lambda) = 0;

};

#endif // IRAWDATAADAPTER_H
