﻿#include "precompiled.h"
#include "SelectedScopeModel.h"

#include "ScopeModel.h"
#include "ThreadModel.h"
#include "IFrameDataModel.h"
#include "ScopeTimingsModel.h"

SelectedScopeModel::SelectedScopeModel(IFrameDataModel& frameDataModel, QObject* parent)
    : QObject(parent)
    , aSelectedTimingId(0)
    , aAnyScopeSelected(false)
    , aSelectedTiming(nullptr)
    , aIsGroup(false)
    , aCallCount(0)
    , aMaxDistance(0.0)
    , aAverageDuration(0.0)
    , aThreadId("")
    , aSelectedScopeName("")
    , aDuration(0.0)
    , mFrameDataModel(frameDataModel)
{
    connect(this, SIGNAL(selectedTimingIdChanged(quint64)), this, SLOT(reactOnSelectedTimingIdChanged(quint64)));
}

void SelectedScopeModel::reactOnSelectedTimingIdChanged(quint64 timingId)
{
    try
    {
        auto foundTimings = mFrameDataModel.findTimingWithId(timingId);
        setAnyScopeSelected(aSelectedTimingId != 0);
        setSelectedTiming(std::get<2>(foundTimings));
        setThreadId(std::get<0>(foundTimings));
        setSelectedScopeName(std::get<1>(foundTimings));
        setDuration(aSelectedTiming->getStop() - aSelectedTiming->getStart());
        setIsGroup(aSelectedTiming->getIsGroup());
    }
    catch(std::invalid_argument)
    {
        zero();
        return;
    }
}

void SelectedScopeModel::zero()
{
    aSelectedTimingId = 0;
    aAnyScopeSelected = false;
    aSelectedTiming = nullptr;
    aThreadId = "";
    aSelectedScopeName = "";
    aDuration = 0.0;

    aIsGroup = false;
    aAverageDuration = 0.0;
    aMaxDistance = 0.0;
    aCallCount = 0.0;
}
