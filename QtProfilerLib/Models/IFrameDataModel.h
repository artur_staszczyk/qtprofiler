﻿#ifndef IFRAMEDATAADAPTER_H
#define IFRAMEDATAADAPTER_H

#include <AutoProperty.h>

#include "Models/IRawDataAdapter.h"
#include "ThreadModel.h"

class IFrameDataModel : public QObject
{
    Q_OBJECT

public:

    IFrameDataModel(QObject* parent = nullptr)
        : QObject(parent)
        , aActiveFrame(0)
        , aFrameStartTime(0.0)
        , aFrameEndTime(0.0)
    {}

    AUTO_PROPERTY(quint32, ActiveFrame, activeFrame)
    AUTO_PROPERTY(double, FrameStartTime, frameStartTime)
    AUTO_PROPERTY(double, FrameEndTime, frameEndTime)

    LIST_PROPERTY(ThreadModel, Threads, threads)

public:

    virtual void setRawDataAdapter(IRawDataAdapter* rawDataAdapter) = 0;
    virtual ThreadModel* findThreadNamed(const QString& name) const = 0;
    virtual std::tuple<QString, QString, ScopeTimingsModel*> findTimingWithId(quint64 timingId) const = 0;

};

#endif // IFRAMEDATAADAPTER_H

