﻿#ifndef RAWDATAADAPTER_H
#define RAWDATAADAPTER_H

#include <QVector>

#include "AutoProperty.h"
#include "IRawDataAdapter.h"

class RawDataAdapter : public IRawDataAdapter
{
    Q_OBJECT

public:

    RawDataAdapter(QObject* parent = nullptr)
        : IRawDataAdapter(parent)
    {}

    Q_PROPERTY(quint32 framesCount READ getFramesCount CONSTANT)
    Q_PROPERTY(double maxFrameDuration READ maxFrameDuration CONSTANT)

    RawDataAdapter&     setRawMeasurementData(const QByteArray& getRawMeasurementsData);
    RawDataAdapter&     setPerFrameDataOffsets(const QVector<quint32>& getDataOffsetForFrame);
    RawDataAdapter&     setPerFrameScopesCount(const QVector<quint32>& getMeasurementCountForFrame);
    RawDataAdapter&     setPerFrameTimings(const QVector<double>& timings);
    RawDataAdapter&     setGlobalScopeNames(const QVector<QString>& scopes);
    RawDataAdapter&     setThreads(const QVector<quint32>& threads);

public:
    quint32     getFramesCount() const override { return mFramesOffsets.size(); }

    Q_INVOKABLE double  getFrameDuration(quint32 frame) const override;
    quint32     getDataOffsetForFrame(quint32 frame) const override;
    quint32     getMeasurementCountForFrame(quint32 frame) const override;

    QByteArray  getRawMeasurementsData() const override { return mRawMeasurementData; }

    quint32     getScopeNamesCount() const override { return mScopeNames.size(); }
    QString     getScopeNameForIndex(quint32 idx) const override;

    quint32     getThreadCount() const override { return mThreads.count(); }
    quint32     getThreadName(quint32 threadIdx) const override { return mThreads[threadIdx]; }

    double      maxFrameDuration() const override;
    void        readFrameMeasurements(quint32 frameId,
                                    std::function< void (double startTime, double endTime, quint32 threadId, quint32 scopeName)> lambda) override;

private:
    QByteArray          mRawMeasurementData;
    QVector<quint32>    mFramesOffsets;
    QVector<quint32>    mFramesMeasurementCount;
    QVector<QString>    mScopeNames;
    QVector<double>     mFramesTimings;
    QVector<quint32>    mThreads;

};

#endif // RAWDATAADAPTER_H
