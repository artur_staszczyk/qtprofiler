﻿#include "ScopeTimingsModel.h"

ScopeTimingsModel::ScopeTimingsModel(QObject *parent)
    : QObject(parent)
    , aUniqueId(reinterpret_cast<quint64>(this))
    , aIsGroup(false)
    , aStart(0.0)
    , aStop(0.0)
    , aAverage(0.0)
    , aCount(0)
    , aMaxDistance(0.0)
{

}
