﻿#ifndef AUTOPROPERTYRELEASE_H
#define AUTOPROPERTYRELEASE_H

#include <QList>
#include <QObject>
#include <QQmlListProperty>
//https://gist.github.com/Rolias/48d453a0490d36090193

#define AUTO_PROPERTY(TYPE, CAPITALIZED_NAME, LOWERED_NAME) \
    Q_PROPERTY(TYPE LOWERED_NAME READ get ## CAPITALIZED_NAME WRITE set ## CAPITALIZED_NAME NOTIFY LOWERED_NAME ## Changed ) \
    public: \
       TYPE get ## CAPITALIZED_NAME() const { return a ## CAPITALIZED_NAME ; } \
       void set ## CAPITALIZED_NAME(TYPE value) { \
          if (a ## CAPITALIZED_NAME == value)  return; \
          a ## CAPITALIZED_NAME = value; \
          emit LOWERED_NAME ## Changed(value); \
        } \
       Q_SIGNAL void LOWERED_NAME ## Changed(TYPE value);\
    protected: \
       TYPE a ## CAPITALIZED_NAME;

#define NON_SETTABLE_PROPERTY(TYPE, CAPITALIZED_NAME, LOWERED_NAME) \
    Q_PROPERTY(TYPE LOWERED_NAME READ get ## CAPITALIZED_NAME NOTIFY LOWERED_NAME ## Changed ) \
    public: \
       TYPE get ## CAPITALIZED_NAME() const { return a ## CAPITALIZED_NAME ; } \
       Q_SIGNAL void LOWERED_NAME ## Changed(TYPE value);\
    protected: \
       TYPE a ## CAPITALIZED_NAME;

#define READONLY_PROPERTY(TYPE, CAPITALIZED_NAME, LOWERED_NAME) \
    Q_PROPERTY(TYPE LOWERED_NAME READ get ## CAPITALIZED_NAME CONSTANT ) \
    public: \
       TYPE get ## CAPITALIZED_NAME() const { return a ## CAPITALIZED_NAME ; } \
    protected: \
       void set ## CAPITALIZED_NAME(TYPE value) {a ## CAPITALIZED_NAME = value; } \
       TYPE a ## CAPITALIZED_NAME;

#define LIST_PROPERTY(TYPE, CAPITALIZED_NAME, LOWERED_NAME) \
    Q_PROPERTY(QQmlListProperty<TYPE> LOWERED_NAME READ get ## CAPITALIZED_NAME NOTIFY LOWERED_NAME ## Changed) \
    public: \
        QQmlListProperty<TYPE> get ## CAPITALIZED_NAME() { return QQmlListProperty<TYPE>(this, a ## CAPITALIZED_NAME ## Raw); } \
        QList<TYPE*> get ## CAPITALIZED_NAME ## Raw() const { return a ## CAPITALIZED_NAME ## Raw; } \
        void add ## CAPITALIZED_NAME(TYPE* value) { \
            a ## CAPITALIZED_NAME ## Raw.append(value); \
            emit LOWERED_NAME ## Changed(QQmlListProperty<TYPE>(this, a ## CAPITALIZED_NAME ## Raw)); \
            emit LOWERED_NAME ## Changed(); \
        } \
        void clear ## CAPITALIZED_NAME() { \
            a ## CAPITALIZED_NAME ## Raw.clear(); \
            emit LOWERED_NAME ## Changed(QQmlListProperty<TYPE>(this, a ## CAPITALIZED_NAME ## Raw)); \
            emit LOWERED_NAME ## Changed(); \
        } \
        void set ## CAPITALIZED_NAME(QList<TYPE*> value) { \
            a ## CAPITALIZED_NAME ## Raw = value; \
            emit LOWERED_NAME ## Changed(QQmlListProperty<TYPE>(this, a ## CAPITALIZED_NAME ## Raw)); \
            emit LOWERED_NAME ## Changed(); \
        } \
        void swap ## CAPITALIZED_NAME(QList<TYPE*>& other) { \
            a ## CAPITALIZED_NAME ## Raw.swap(other); \
            emit LOWERED_NAME ## Changed(QQmlListProperty<TYPE>(this, a ## CAPITALIZED_NAME ## Raw)); \
            emit LOWERED_NAME ## Changed(); \
        } \
        Q_SIGNAL void LOWERED_NAME ## Changed(QQmlListProperty<TYPE> value); \
        Q_SIGNAL void LOWERED_NAME ## Changed(); \
    protected: \
        QList<TYPE*> a ## CAPITALIZED_NAME ## Raw;


#endif // AUTOPROPERTYRELEASE_H
