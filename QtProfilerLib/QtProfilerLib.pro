#-------------------------------------------------
#
# Project created by QtCreator 2015-04-20T23:04:21
#
#-------------------------------------------------
include(../include.pri)

CONFIG(debug, debug|release): LIB_NAME = profilerLibd
CONFIG(release, debug|release): LIB_NAME = profilerLib

QT += qml widgets
QT       -= gui

TEMPLATE = lib
CONFIG += staticlib precompile_header
TARGET = $$LIB_NAME
PRECOMPILED_HEADER = precompiled.h

INCLUDEPATH += $$PWD/../QtMeasureLib

SOURCES += \
    Models/FrameDataModel.cpp \
    Models/RawDataAdapter.cpp \
    FileLoading/FileData.cpp \
    FileLoading/FileLoader.cpp \
    FileLoading/FileDataConverter.cpp \
    Models/SelectedScopeModel.cpp \
    Models/ScopeTimingsModel.cpp \
    Models/ScopeModel.cpp \
    Models/ThreadModel.cpp \
    Helpers.cpp

HEADERS += \
    Models/FrameDataModel.h \
    Models/IFrameDataModel.h \
    Models/RawDataAdapter.h \
    FileLoading/FileLoader.h \
    FileLoading/FileData.h \
    FileLoading/FileDataConverter.h \
    Exceptions.h \
    AutoProperty.h \
    Models/IRawDataAdapter.h \
    Models/SelectedScopeModel.h \
    Models/ScopeTimingsModel.h \
    Models/ScopeModel.h \
    Models/ThreadModel.h \
    AutoPropertyRelease.h \
    AutoPropertyTests.h \
    Helpers.h

target.path = $$PWD/../libs-qt
INSTALLS += target
