﻿#ifndef FILELOADER_H
#define FILELOADER_H

#include <QObject>
#include <memory>

class FileConverterStrategy;

class FileLoader : public QObject
{
    Q_OBJECT
public:
    explicit FileLoader(QObject *parent = 0);

    QByteArray loadFile(const QString& fileName);

private:
    std::shared_ptr<FileConverterStrategy> mFileConverter;
};

#endif // FILELOADER_H
