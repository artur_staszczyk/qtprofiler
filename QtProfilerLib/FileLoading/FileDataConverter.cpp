﻿#include "precompiled.h"

#include "FileDataConverter.h"
#include "Models/RawDataAdapter.h"

const quint64 FileDataConverter::HEADER_VERSION_1 = 0xEEEEEEEE00000001;
const quint64 FileDataConverter::HEADER_VERSION_2 = 0xEEEEEEEE00000002;

const quint64 FileDataConverter::FRAME_END_MARKER = 0x00000000FFFFFFFF;

IRawDataAdapter* FileDataConverter::convert(const QByteArray &byteArray)
{
    QDataStream dataStream(byteArray);
    dataStream.setByteOrder(QDataStream::LittleEndian);

    verifyHeader(dataStream);
    readProfileData(dataStream);
    readFrames();
    readScopes(dataStream);

    auto ret = new RawDataAdapter();
    ret->setRawMeasurementData(mRawMeasurementData).
            setPerFrameDataOffsets(mFrameOffsets).
            setPerFrameScopesCount(mFrameScopeCounts).
            setPerFrameTimings(mFrameTimings).
            setGlobalScopeNames(mScopeNames).
            setThreads(mThreads);
    return ret;
}

void FileDataConverter::verifyHeader(QDataStream& dataStream)
{
    quint64 readHeaderVersion;
    dataStream >> readHeaderVersion;

    if(readHeaderVersion != mExpectedHeaderVersion)
    {
        std::stringstream errorString;
        errorString << "Bad file version. Expected [" << mExpectedHeaderVersion <<
                       "], Profided [" << readHeaderVersion;
        throw bad_version(errorString.str());
    }
}

void FileDataConverter::readProfileData(QDataStream& dataStream)
{
    quint64 dataSize;
    dataStream >> dataSize;

    // TODO: invalid operation
    char* readBuffer = new char[dataSize];
    quint32 readBytes = dataStream.readRawData(readBuffer, dataSize);
    mRawMeasurementData.append(QByteArray::fromRawData(readBuffer, readBytes));
    delete [] readBuffer;

    if(readBytes < dataSize)
        throw std::out_of_range("Invalid input data range");
}

quint32 FileDataConverter::threadIdFromComplexValue(quint64 value)
{
    return (value & 0x00000000FFFFFFFF);
}

quint32 FileDataConverter::scopeIdFromComplexValue(quint64 value)
{
    return (value >> 32);
}

void FileDataConverter::readFrames()
{
    QDataStream stream(mRawMeasurementData);
    stream.setByteOrder(QDataStream::LittleEndian);

    quint32 frameOffset = 0;
    quint32 frameScopeCount = 0;

    double minFrameTime = std::numeric_limits<double>::max();
    double maxFrameTime = std::numeric_limits<double>::min();

    mFrameOffsets.push_back(frameOffset);
    while(!stream.atEnd())
    {
        quint64 scopeFirstData;
        stream >> scopeFirstData;

        if(scopeFirstData == FileDataConverter::FRAME_END_MARKER)
        {
            mFrameScopeCounts.push_back(frameScopeCount);
            frameScopeCount = 0;

            mFrameOffsets.push_back(++frameOffset);
            mFrameTimings.push_back(maxFrameTime - minFrameTime);

            minFrameTime = std::numeric_limits<double>::max();
            maxFrameTime = std::numeric_limits<double>::min();
        }
        else
        {
            double scopeStartTime, scopeEndTime;
            quint64 scopeId, threadId, complexValue;

            scopeStartTime = *reinterpret_cast<double*>(&scopeFirstData);
            stream >> scopeEndTime >> complexValue;

            scopeId = scopeIdFromComplexValue(complexValue);
            threadId = threadIdFromComplexValue(complexValue);

            if(std::find(mThreads.begin(), mThreads.end(), threadId) == mThreads.end())
                mThreads.push_back(threadId);

            if(scopeStartTime < minFrameTime)
                minFrameTime = scopeStartTime;

            if(scopeEndTime > maxFrameTime)
                maxFrameTime = scopeEndTime;

            frameScopeCount++;
            frameOffset+=3;
        }
    }

    if(frameScopeCount > 0)
    {
        mFrameScopeCounts.push_back(frameScopeCount);
        mFrameTimings.push_back(maxFrameTime - minFrameTime);
    }
    else
    {
        mFrameOffsets.pop_back();
    }
}

void FileDataConverter::readScopes(QDataStream& dataStream)
{
    quint64 scopesCount;
    dataStream >> scopesCount;

    for(quint32 i = 0; i < scopesCount; ++i)
    {
        quint8 scopeNameSize;
        dataStream >> scopeNameSize;

        char* scopeNamePtr = new char[scopeNameSize + 1];
        dataStream.readRawData(scopeNamePtr, scopeNameSize);
        scopeNamePtr[scopeNameSize] = '\0';

        QString scopeName(scopeNamePtr);
        mScopeNames.push_back(scopeName);

        delete [] scopeNamePtr;
    }
}
