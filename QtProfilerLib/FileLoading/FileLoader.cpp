﻿#include "FileLoader.h"

#include <QFile>
#include <QDebug>
#include <QVector>
#include <QString>
#include <QDataStream>

FileLoader::FileLoader(QObject *parent) : QObject(parent)
{

}

QByteArray FileLoader::loadFile(const QString& fileName)
{
    qDebug() << "Opening file: " << fileName;
    QFile profileFile(fileName);
    if(!profileFile.exists())
        throw std::runtime_error("File does not exists");

    profileFile.open(QIODevice::ReadOnly);
    if(profileFile.size() == 0)
        throw std::runtime_error("File is empty");

    QByteArray fileData = profileFile.readAll();
    return fileData;
}
