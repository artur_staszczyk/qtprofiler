﻿#ifndef FILECONVERTERVERSION1_H
#define FILECONVERTERVERSION1_H

#include <QVector>

#include "Models/RawDataAdapter.h"

class FileDataConverter : public QObject
{
    Q_OBJECT
public:
    static const quint64 HEADER_VERSION_1;
    static const quint64 HEADER_VERSION_2;

    static const quint64 FRAME_END_MARKER;

public:
    FileDataConverter(quint64 expectedHeaderVersion)
        : mExpectedHeaderVersion(expectedHeaderVersion)
    {}

    IRawDataAdapter* convert(const QByteArray &byteArray);

    static quint32 threadIdFromComplexValue(quint64 value);
    static quint32 scopeIdFromComplexValue(quint64 value);

protected:
    void            verifyHeader(QDataStream& dataStream);

    void            readProfileData(QDataStream& dataStream);
    void            readFrames();
    void            readScopes(QDataStream& dataStream);



private:
    quint64 mExpectedHeaderVersion;

    QByteArray mRawMeasurementData;
    QVector<quint32> mFrameOffsets;
    QVector<double> mFrameTimings;
    QVector<quint32> mFrameScopeCounts;
    QVector<QString> mScopeNames;
    QVector<quint32> mThreads;
};

#endif // FILECONVERTERVERSION1_H
