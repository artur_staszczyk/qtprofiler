﻿#include "precompiled.h"

#include <QDebug>
#include <QFileDialog>

#include "FileData.h"
#include "FileLoading/FileLoader.h"

FileData::FileData(QObject *parent) : QObject(parent)
{

}

FileData::~FileData()
{

}

void FileData::openProfileFile(QUrl fileUrl)
{
    openProfileFile(fileUrl.toLocalFile());
}

void FileData::openProfileFile(QString fileName)
{
    FileLoader fileLoader;
    mFileAsByteArray = fileLoader.loadFile(fileName);
    mHeader = readHeader();

    emit fileDidLoad(fileName);
}

quint64 FileData::readHeader()
{
    QDataStream dataStream(mFileAsByteArray);
    dataStream.setByteOrder(QDataStream::LittleEndian);
    quint64 header;
    dataStream >> header;

    return header;
}
