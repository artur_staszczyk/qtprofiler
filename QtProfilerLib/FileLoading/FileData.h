﻿#ifndef PROFILEDATA_H
#define PROFILEDATA_H

#include <memory>

#include <QUrl>
#include <QObject>
#include <QVector>
#include <QString>

class FileData : public QObject
{
    Q_OBJECT
public:
    explicit FileData(QObject *parent = 0);
    virtual ~FileData();

    quint64 getHeader() const { return mHeader; }
    const QByteArray& getData() const { return mFileAsByteArray; }

public slots:
    void openProfileFile(QUrl fileUrl);
    void openProfileFile(QString fileName);

signals:
    void fileDidLoad(const QString& fileName);

private:
    QByteArray mFileAsByteArray;
    quint64 mHeader;

    quint64 readHeader();
};

#endif // PROFILEDATA_H
