DEFINES += QT
CONFIG(debug, debug|release): DEFINES += DEBUG

win32: DEFINES += WINDOWS
else:mac: DEFINES += MAC UNIX
else:unix: DEFINES += UNIX

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#win32: {
#    INCLUDEPATH += $$(GOOGLE_TEST)/include $$(GOOGLE_MOCK)/include
#    LIBS += -L$$(GOOGLE_TEST)/msvc/gtest-md/Debug/ -L$$(GOOGLE_MOCK)/msvc/2015/Debug -lgtestd -lgmock
#}
unix|mac: {
    INCLUDEPATH += /usr/local/include
    LIBS += -L/usr/local/lib -lgoogletest -lgooglemock
}

macx: {
    CONFIG+=strict_c++ c++14
}
