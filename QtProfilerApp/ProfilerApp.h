﻿#ifndef PROFILERAPP_H
#define PROFILERAPP_H

#include <QObject>
#include <QQmlApplicationEngine>

class FileData;
class CppModels;
class IRawDataAdapter;
class IFrameDataModel;

class ProfilerApp : public QObject
{
    Q_OBJECT
public:
    explicit ProfilerApp(QQmlApplicationEngine* engine, QObject* parent = nullptr);
    ~ProfilerApp();

    void load();

private slots:
    void reactOnFileLoaded(const QString&);

private:
    QQmlApplicationEngine*  mQmlEngine;
    FileData*               mFileData;

    CppModels*              mCppModels;
    IFrameDataModel*        mFrameDataModel;
    IRawDataAdapter*        mRawDataAdapter;
};

#endif // PROFILERAPP_H
