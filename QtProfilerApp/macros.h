﻿#ifndef MACROS_H
#define MACROS_H

#define QML_CALL // empty method marker that this signal/slot is called directly from QML

// Empty method markers for .cpp files
#define STATIC_PUBLIC_METHODS
#define Q_INVOKABLE_METHODS
#define PROTECTED_METHODS
#define PRIVATE_METHODS
#define PROTECTED_SLOTS
#define PUBLIC_METHODS
#define PRIVATE_SLOTS
#define PUBLIC_SLOTS

#endif // MACROS_H
