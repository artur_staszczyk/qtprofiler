﻿import QtQuick 2.7

Item {
    id: fakeModels

    property var fakeFrameTimings: [1, 1.5, 1.2]

    property var fakeScopeNamesPerFrame: [
        ["Scope 1", "Scope 2"],
        ["Scope 1", "Scope 3"],
        ["Scope 1", "Scope 2", "Scope 3"]
    ]

    property QtObject root: QtObject {

        function openFile(name)
        {
            console.log("open another file: " + name)
        }

        function findScopeInThread(timingId)
        {
            for(var tid = 0; tid < frame.threads.count; tid++)
            {
                var threadData = frame.threads.get(tid)
                var scopeList = threadData.scopes
                for(var sid = 0; sid < scopeList.count; sid++)
                {
                    var scopeData = scopeList.get(sid)
                    var timingsList = scopeData.timings
                    for(var timId = 0; timId < timingsList.count; timId++)
                    {
                        if(timingsList.get(timId).uniqueId === timingId)
                            return [threadData.threadId, scopeData.itemName, timingsList.get(timId)]
                    }
                }
            }

            return null
        }

        property QtObject frame: QtObject {

            property var uniqueScopes: fakeModels.fakeScopeNamesPerFrame[activeFrame - 1]
            property int activeFrame: 1

            property real frameStartTime: 0
            property real frameEndTime: 1

            property var threads: ListModel {
                    ListElement {
                        threadId: "Thread 1"
                        collapsed: true

                        scopes: [
                            ListElement { itemName: "Scope 1"; timings: [
                                    ListElement { uniqueId: 1; isGroup: true; start: 0.1; stop: 0.2; average: 0.03; count: 7; maxDistance: 0.01 },
                                    ListElement { uniqueId: 2; isGroup: false; start: 0.4; stop: 0.7 }
                                ] },
                            ListElement { itemName: "Scope 2"; timings: [
                                    ListElement { uniqueId: 3; isGroup: false; start: 0.4; stop: 0.9 }
                                ] }
                        ]
                    }

                    ListElement {
                        threadId: "Thread 2"
                        collapsed: true
                        scopes: [
                            ListElement { itemName: "Scope 1"; timings: [
                                    ListElement { uniqueId: 4; isGroup: true; start: 0.1; stop: 0.2; average: 0.03; count: 7; maxDistance: 0.01 },
                                    ListElement { uniqueId: 5; isGroup: false; start: 0.4; stop: 0.7 }
                                ] },
                            ListElement { itemName: "Scope 2"; timings: [
                                    ListElement { uniqueId: 6; isGroup: false; start: 0.4; stop: 0.9 }
                                ] }
                        ]
                    }

                    ListElement {
                        threadId: "Thread 3"
                        collapsed: true
                        scopes: [
                        ]
                    }
                }

            onActiveFrameChanged: {
                fakeModels.root.selectedScope.selectedScopeName = ""
            }
        }

        property QtObject rawData: QtObject {

            property int framesCount: 3
            property int maxFrameDuration: 2

            function getFrameDuration(frameNo)
            {
                return fakeModels.fakeFrameTimings[frameNo]
            }
        }

        property QtObject selectedScope: QtObject {

            property bool anyScopeSelected: selectedScopeName !== ''
            property int selectedTimingId: 0

            property string threadId: ''
            property string selectedScopeName: ''
            property int callNumber: 4

            property real duration: 0.0

            property bool isGroup: true
            property int callCount: 0
            property real averageDuration: 0.0
            property real maxDistance: 0.0

            onSelectedTimingIdChanged: {
                fakeUpdateSelectedScope()
            }

            function fakeUpdateSelectedScope()
            {
                var data = fakeModels.root.findScopeInThread(selectedTimingId)

                threadId = data[0]
                selectedScopeName = data[1]

                var timing = data[2]
                duration = timing.stop - timing.start
                anyScopeSelected = true
                isGroup = timing.isGroup
                if(timing.isGroup)
                {
                    averageDuration = timing.average
                    callCount = timing.count
                    maxDistance = timing.maxDistance
                }

            }

        }

    }

}
