﻿import QtQuick 2.0

import "Commons.js" as Commons

Rectangle {
    id: element

    signal selectedFrame(int index)

    property bool highlighted: index === (_models.frame.activeFrame - 1)

    color: Commons.colorInvisible
    border.color: Commons.defaultBorderColor
    border.width: highlighted ? 2 : 0

    Rectangle {

        width: parent.width - 2
        height: Math.floor(_models.rawData.getFrameDuration(index) / _models.rawData.maxFrameDuration * (parent.height))

        x: 1
        y: parent.height - height - 1

        color: Commons.defaultClickableColor
    }

    Text {
        id: elementName
        text: "#" + (index + 1) + " [" + _models.rawData.getFrameDuration(index).toFixed(2) + " ms]"
        rotation: -90
        anchors.fill: parent

        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter

        color: Commons.defaultTextColor
        font.family: Commons.defaultFont
        font.pointSize: 9

        antialiasing: true

        states: State {
            name: "hovered"; when: elementMouseArea.hovered || element.highlighted
            PropertyChanges {
                target: elementName
                font.bold: true
            }
        }
    }

    MouseArea {
        id: elementMouseArea

        anchors.fill: parent
        hoverEnabled: true

        property bool hovered: false

        onPressed: {
            element.selectedFrame(index)
        }

        onEntered: hovered = true
        onExited: hovered = false
    }
}
