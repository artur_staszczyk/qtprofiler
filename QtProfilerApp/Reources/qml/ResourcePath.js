﻿function resolve(relativePath, name, faked) {
    if(faked)
        return name
    else
        return relativePath + "/" + name
}
