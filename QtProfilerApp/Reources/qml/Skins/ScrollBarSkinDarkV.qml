﻿import QtQuick 2.0
import QtQuick.Controls.Styles 1.1

ScrollViewStyle {

    property int scrollBarWidth

    scrollBarBackground: Item {
        implicitWidth: scrollBarWidth
        Rectangle {
            color: "#3E3E42"

            anchors.fill: parent
        }
    }

    decrementControl: Item {
        implicitHeight: scrollBarWidth
        implicitWidth: scrollBarWidth
        Rectangle {
            anchors.fill: parent
            color: "#3E3E42"
            Image {
                id: decrementImg
                anchors.fill: parent
                source: "../../ui/scroll_decrement.png"
                rotation: 90
            }

            states: [
                State {
                    name: "hovered"; when: (styleData.hovered && !styleData.pressed);
                    PropertyChanges { target: decrementImg;
                        source: "../../ui/scroll_decrement_h.png"; }
                },

                State {
                    name: "pressed"; when: styleData.pressed;
                    PropertyChanges { target: decrementImg;
                        source: "/ui/Reources/ui/scroll_decrement_p.png" }
                }
            ]
        }
    }

    incrementControl: Item {
        implicitHeight: scrollBarWidth
        implicitWidth: scrollBarWidth
        Rectangle {
            anchors.fill: parent
            color: "#3E3E42"
            Image {
                id: incrementImg
                anchors.fill: parent
                source: "../../ui/scroll_decrement.png"
                rotation: -90
            }

            states: [
                State {
                    name: "hovered"; when: (styleData.hovered && !styleData.pressed);
                    PropertyChanges { target: incrementImg;
                        source: "/ui/Reources/ui/scroll_decrement_h.png"; }
                },

                State {
                    name: "pressed"; when: styleData.pressed;
                    PropertyChanges { target: incrementImg;
                        source: "/ui/Reources/ui/scroll_decrement_p.png" }
                }
            ]
        }
    }

    handle: Item {
        implicitWidth: scrollBarWidth
        implicitHeight: 30

        Rectangle {
            id: handleItem
            color: "#686868"
            anchors.fill: parent
            anchors.topMargin: 2
            anchors.leftMargin: 4
            anchors.rightMargin: 4
            anchors.bottomMargin: 2

            states: [
                State {
                    name: "hovered"; when: (styleData.hovered && !styleData.pressed);
                    PropertyChanges { target: handleItem; color: "#9E9E9E"; }
                },
                State {
                    name: "pressed"; when: styleData.pressed;
                    PropertyChanges { target: handleItem; color: "#D0D0D0" }
                }
            ]

            transitions: [
                Transition {
                    ColorAnimation { properties: "color"; duration: 70 }
                }
            ]
        }
    }
}
