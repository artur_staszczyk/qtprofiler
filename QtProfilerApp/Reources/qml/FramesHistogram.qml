﻿import QtQuick 2.0
import QtQuick.Controls 1.3

import "Skins"

Item {

    id: framesHistogram

    signal selectedFrame(int index)

    property int elementWidth: 25
    property int elementSkipCount: 6

    property alias color: background.color
    property alias border: background.border

    Rectangle {
        id: background

        anchors.fill: parent
    }

    ScrollView {
        anchors {
            fill: parent
            topMargin: 1
            leftMargin: 1
            rightMargin: 1
        }

        ListView {
            id: histogramListView

            model: _models.rawData.framesCount
            delegate: FramesHistogramElement {
                width: elementWidth
                height: parent.height

                onSelectedFrame: framesHistogram.selectedFrame(index)
            }

            orientation: Qt.Horizontal
            boundsBehavior: Flickable.StopAtBounds
        }

        style: ScrollBarSkinDarkH {
            scrollBarHeight: 15
        }
    }

//    Connections {
//            target: _models.frame
//            onActiveFrameChanged: scrollViewportToElement(_models.frame.activeFrame)
//    }

//    function scrollViewportToElement(activeFrame) {
//        var activeFrameX1 = (activeFrame - 1) * elementWidth
//        var activeFrameX2 = (activeFrame) * elementWidth

//        if(histogramListView.contentX > activeFrameX1)
//            histogramListView.contentX -= elementSkipCount * elementWidth

//        if(histogramListView.contentX + histogramListView.width < activeFrameX2)
//            histogramListView.contentX += elementSkipCount * elementWidth
//    }
}
