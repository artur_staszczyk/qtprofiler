﻿import QtQuick 2.0

import "Commons.js" as Commons

Item {

    property alias color: background.color
    property alias border: background.border

    property alias frame: frameValue.text
    property alias threadId: threadIdValue.text
    property alias scopeName: scopeNameValue.text
    property alias timingIndex: measurementIndexValue.text
    property alias duration: measurementDurationValue.text
    property alias averageDuration: measurementAverageDurationValue.text
    property alias callCount: measurementCallCountValue.text
    property alias maxDistanceBetween: measurementMaxDistanceBetweenValue.text


    Rectangle {
        id: background
        anchors.fill: parent
    }

    Column {
        id: headersColumn

        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.margins: 2

        width: parent.width * 0.4

        Text {
            text: "Frame:"
            font.family: Commons.defaultFont
            font.pointSize: Commons.defaultTextSize
            color: Commons.defaultTextColor
        }

        Text {
            text: "Thread:"
            font.family: Commons.defaultFont
            font.pointSize: Commons.defaultTextSize
            color: Commons.defaultTextColor
            enabled: _models.selectedScope.anyScopeSelected
            visible: enabled
        }

        Text {
            text: "Scope:"
            font.family: Commons.defaultFont
            font.pointSize: Commons.defaultTextSize
            color: Commons.defaultTextColor
            enabled: _models.selectedScope.anyScopeSelected
            visible: enabled
        }

        Text {
            text: "Call #:"
            font.family: Commons.defaultFont
            font.pointSize: Commons.defaultTextSize
            color: Commons.defaultTextColor
            enabled: _models.selectedScope.anyScopeSelected
            visible: enabled
        }

        Text {
            text: "Duration:"
            font.family: Commons.defaultFont
            font.pointSize: Commons.defaultTextSize
            color: Commons.defaultTextColor
            enabled: _models.selectedScope.anyScopeSelected
            visible: enabled
        }

        Text {
            text: "Average duration:"
            font.family: Commons.defaultFont
            font.pointSize: Commons.defaultTextSize
            color: Commons.defaultTextColor
            enabled: _models.selectedScope.isGroup && _models.selectedScope.anyScopeSelected
            visible: enabled
        }

        Text {
            text: "Call count:"
            font.family: Commons.defaultFont
            font.pointSize: Commons.defaultTextSize
            color: Commons.defaultTextColor
            enabled: _models.selectedScope.isGroup && _models.selectedScope.anyScopeSelected
            visible: enabled
        }

        Text {
            text: "Max distance between calls:"
            font.family: Commons.defaultFont
            font.pointSize: Commons.defaultTextSize
            color: Commons.defaultTextColor
            enabled: _models.selectedScope.isGroup && _models.selectedScope.anyScopeSelected
            visible: enabled
        }
    }

    Column {
        id: valuesColumn

        anchors.left: headersColumn.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 2

        Text {
            id: frameValue
            text: "NONE"
            width: parent.width

            horizontalAlignment: Text.AlignRight
            font.family: Commons.defaultFont
            font.pointSize: Commons.defaultTextSize
            color: Commons.defaultTextColor
        }

        Text {
            id: threadIdValue
            text: "NONE"
            width: parent.width

            horizontalAlignment: Text.AlignRight
            font.family: Commons.defaultFont
            font.pointSize: Commons.defaultTextSize
            color: Commons.defaultTextColor

            enabled: _models.selectedScope.anyScopeSelected
            visible: enabled
        }

        Text {
            id: scopeNameValue
            text: "NONE"
            width: parent.width

            horizontalAlignment: Text.AlignRight
            font.family: Commons.defaultFont
            font.pointSize: Commons.defaultTextSize
            color: Commons.defaultTextColor

            enabled: _models.selectedScope.anyScopeSelected
            visible: enabled
        }


        Text {
            id: measurementIndexValue
            text: "NONE"
            width: parent.width
            horizontalAlignment: Text.AlignRight
            font.family: Commons.defaultFont
            font.pointSize: Commons.defaultTextSize
            color: Commons.defaultTextColor

            enabled: _models.selectedScope.anyScopeSelected
            visible: enabled
        }

        Text {
            id: measurementDurationValue
            text: "NONE"
            width: parent.width
            horizontalAlignment: Text.AlignRight
            font.family: Commons.defaultFont
            font.pointSize: Commons.defaultTextSize
            color: Commons.defaultTextColor

            enabled: _models.selectedScope.anyScopeSelected
            visible: enabled
        }

        Text {
            id: measurementAverageDurationValue
            text: "NONE"
            width: parent.width
            horizontalAlignment: Text.AlignRight
            font.family: Commons.defaultFont
            font.pointSize: Commons.defaultTextSize
            color: Commons.defaultTextColor
            enabled: _models.selectedScope.isGroup && _models.selectedScope.anyScopeSelected
            visible: enabled
        }

        Text {
            id: measurementCallCountValue
            text: "NONE"
            width: parent.width
            horizontalAlignment: Text.AlignRight
            font.family: Commons.defaultFont
            font.pointSize: Commons.defaultTextSize
            color: Commons.defaultTextColor
            enabled: _models.selectedScope.isGroup && _models.selectedScope.anyScopeSelected
            visible: enabled
        }

        Text {
            id: measurementMaxDistanceBetweenValue
            text: "NONE"
            width: parent.width
            horizontalAlignment: Text.AlignRight
            font.family: Commons.defaultFont
            font.pointSize: Commons.defaultTextSize
            color: Commons.defaultTextColor
            enabled: _models.selectedScope.isGroup && _models.selectedScope.anyScopeSelected
            visible: enabled
        }
    }

}
