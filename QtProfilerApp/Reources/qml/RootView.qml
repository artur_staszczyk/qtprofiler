﻿import QtQuick 2.8
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.0

import "ScopeRenderer"
import "Commons.js" as Commons
import "ResourcePath.js" as Resources

ApplicationWindow {

    id: window

    property bool fakeCpp: true
    property bool isDesktop: Qt.platform.os === "osx" || Qt.platform.os === "windows"
    property bool isCppAvailable: (typeof(_cppModels) !== "undefined")

    property QtObject _models: isCppAvailable ? _cppModels : fakeModelsLoader.item.root

    visible: true
    width: 667
    height: 375

    Material.accent: Material.Cyan
    Material.theme: Material.Light
    color: Commons.defaultWindowColor

    Loader {
       id: fakeModelsLoader

       active: !window.isCppAvailable

       source: Resources.resolve("../fakes", "FakeCpp.qml", isCppAvailable)
    }

    header: ToolBar {
        RowLayout {
            anchors.fill: parent

            ToolButton {
                text: "Load file"
                onClicked: openFileDialog.visible = true
            }
        }
    }

    FileDialog {
        id: openFileDialog

        title: "Load new profile file"
        folder: shortcuts.home

        onAccepted: {
            _models.openFile(openFileDialog.fileUrl)
        }

        Component.onCompleted: visible = false
    }

    Row {
        id: upperRow

        spacing: 5
        height: 160
        anchors.left: parent.left
        anchors.right: parent.right

        SelectionDetailsView {
            id: selectionDetailsView

            threadId: _models.selectedScope.threadId
            scopeName: _models.selectedScope.selectedScopeName
            timingIndex: _models.selectedScope.callNumber
            duration: _models.selectedScope.duration.toFixed(2)
            averageDuration: _models.selectedScope.averageDuration.toFixed(2)
            maxDistanceBetween: _models.selectedScope.maxDistance.toFixed(2)
            callCount: _models.selectedScope.callCount

            width: 260
            height: parent.height

            color: Commons.defaultBackgroundColor
            border.color: Commons.defaultBorderColor
            border.width: 1

            frame: activeFrameDisplay();
        }

        FramesHistogram {
            id: framesHistogram

            width: parent.width - selectionDetailsView.width - upperRow.spacing
            height: parent.height

            color: Commons.defaultBackgroundColor
            border.color: Commons.defaultBorderColor
            border.width: 1

            onSelectedFrame: {
                _models.frame.activeFrame = index + 1
                selectionDetailsView.frame = activeFrameDisplay()
            }
        }
    }

    ScopeMeasurementsRenderer {
        id: scopeMeasurementsRenderer

        anchors.top: upperRow.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.topMargin: 5

        color: Commons.defaultBackgroundColor
        border.color: Commons.defaultBorderColor
        border.width: 1
    }

    function activeFrameDisplay() {
        return _models.frame.activeFrame + " / " + _models.rawData.framesCount
    }

}
