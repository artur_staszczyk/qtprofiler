﻿.pragma library

var defaultBackgroundColor = "#1E1E1E";

var defaultBorderColor = "#a0a0a0";
var redBorderColor = "#faa0a0";
var vagueBorderColor = "black"

var defaultWindowColor = "#2d2d30"

var defaultClickableColor = "#3399FF"
var selectedClicableColor = "#55BBFF"

var colorInvisible = "transparent"

// TEXT
var defaultFont = "Arial"
var defaultTextColor = "#E6FFFF"
var defaultTextSize = 12
var captionTextSize = 18
