﻿import QtQuick 2.0

Item {
    property alias model: mainList.model
    property alias boundsBehavior: mainList.boundsBehavior

    ListView {
        anchors.fill: parent
        anchors.margins: 1
        id: mainList

        spacing: 2

        delegate: ExpandableThreadCaption{}
    }

}
