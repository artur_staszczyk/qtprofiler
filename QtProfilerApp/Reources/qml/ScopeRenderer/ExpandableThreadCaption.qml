﻿import QtQuick 2.0
import ".."
import "../Commons.js" as Commons
import "../ResourcePath.js" as Resources

Component {

    Column {
        width: parent.width

        Rectangle {
            id: categoryItem
            border.color: Commons.defaultBorderColor
            border.width: 0
            color: Commons.defaultWindowColor
            height: 30
            radius: 0
            width: parent.width

            Text {
                anchors.verticalCenter: parent.verticalCenter
                x: 15

                font.family: Commons.defaultFont
                font.pixelSize: Commons.captionTextSize
                text: threadId
                color: Commons.defaultTextColor
            }

            Rectangle {
                color: Commons.defaultBorderColor
                opacity: collapsed ? 0.7 : 1

                width: height
                height: parent.height - 6
                radius: 4

                anchors.right: parent.right
                anchors.rightMargin: 15
                anchors.verticalCenter: parent.verticalCenter

                Image {
                    anchors.fill: parent
                    anchors.margins: 5
                    source: Resources.resolve("../../resources", "/icons-design-app-ui/papers9.png", isCppAvailable)
                }

                MouseArea {
                    anchors.fill: parent

                    // Toggle the 'collapsed' property
                    onClicked: collapsed = !collapsed// nestedModel.setProperty(index, "collapsed", !collapsed)
                }
            }
        }

        Loader {
            id: subItemLoader
            width: parent.width


            // This is a workaround for a bug/feature in the Loader element. If sourceComponent is set to null
            // the Loader element retains the same height it had when sourceComponent was set. Setting visible
            // to false makes the parent Column treat it as if it's height was 0.
            visible: !collapsed
            source: collapsed ? "" : "ScopesInThreadRenderer.qml"
            onStatusChanged: if (status == Loader.Ready && item !== null) item.model = scopes
        }

    }

}
