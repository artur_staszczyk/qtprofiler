﻿import QtQuick 2.7
import "../Commons.js" as Commons
import ".."

Column {

    property alias model: scopeRepeater.model

    Repeater
    {
        id: scopeRepeater

        delegate: ScopeMeasurementElement {}

    }
}
