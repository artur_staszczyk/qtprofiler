﻿import QtQuick 2.3
import QtQuick.Controls 1.2

import "../Magnifier"

Item
{
    property alias color: background.color
    property alias border: background.border

    Rectangle {
        id: background
        anchors.fill: parent
    }

    ExpandableList {
        id: measurementsList

        anchors.fill: parent
        clip: true

        boundsBehavior: Flickable.StopAtBounds

        model: _models.frame.threads
    }

////    https://developer.ubuntu.com/en/blog/2015/04/28/magnifying-glass-qml/
////    Magnifier {
////        anchors.fill: parent
////        scene: scroll
////        area: mouseArea

}

