﻿import QtQuick 2.8

import "../Tooltip"
import "../Commons.js" as Commons

Row {
    id: scopeMeasurementElement

    property real frameTime: _models.frame.frameEndTime - _models.frame.frameStartTime
    property int graphWidth: parent.width - scopeTitle.width - scopeMeasurementElement.x

    x: 20

    Text {
        id: scopeTitle
        width: 240
        height: 30
        text: itemName

        color: Commons.defaultTextColor
        font.family: Commons.defaultFont
        font.pointSize: Commons.defaultTextSize

        antialiasing: true

        verticalAlignment: Text.AlignVCenter
        clip: true
    }

    Rectangle {
        width: 1
        height: scopeTitle.height
        color: Commons.defaultWindowColor
    }

    Item {
        Repeater {

            model: timings

            delegate: Rectangle {
                id: measurement

                property var selected: _models.selectedScope.selectedScopeName === itemName &&
                                       _models.selectedScope.callNumber - 1 == index

                width: Math.max((stop - start) / frameTime * graphWidth, 6)
                height: scopeTitle.height
                x: scopeTitle.width + ((start - _models.frame.frameStartTime)/ frameTime * graphWidth)
                y: 0

                radius: 4
                border.width: selected ? 2 : 1
                border.color: isGroup ? Commons.redBorderColor : Commons.vagueBorderColor

                color: selected ? Commons.selectedClicableColor : Commons.defaultClickableColor

                Text {
                    id: scopeNameText

                    text: "[" + (stop - start).toFixed(2) + ' ms]'
                    anchors.centerIn: parent

                    color: Commons.defaultTextColor
                    font.family: Commons.defaultFont
                    font.pointSize: Commons.defaultTextSize

                    antialiasing: true

                    visible: scopeNameText.contentWidth < parent.width - 6
                }

    ////            ToolTipArea {
    ////                showDelay: 100
    ////                text: scopeNameText.text
    ////            }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        _models.selectedScope.selectedTimingId = uniqueId
                        _models.selectedScope.callNumber = index + 1
                    }
                }

            }
        }
    }
}
