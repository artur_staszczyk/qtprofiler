﻿#include <QQmlContext>

#include <FileLoading/FileData.h>
#include <FileLoading/FileDataConverter.h>
#include <Models/FrameDataModel.h>
#include <Models/SelectedScopeModel.h>
#include <Models/ThreadModel.h>

#include "ProfilerApp.h"
#include "Models/CppModels.h"

ProfilerApp::ProfilerApp(QQmlApplicationEngine* engine, QObject* parent)
    : QObject(parent)
    , mQmlEngine(engine)
    , mFileData(new FileData(this))
    , mCppModels(nullptr)
    , mFrameDataModel(nullptr)
    , mRawDataAdapter(nullptr)
{
    mCppModels = new CppModels(this);
    mQmlEngine->setParent(this);
}

ProfilerApp::~ProfilerApp()
{
    //delete mQmlEngine;
}

void ProfilerApp::load()
{
    qRegisterMetaType<IFrameDataModel*>();
    qRegisterMetaType<IRawDataAdapter*>();
    qmlRegisterUncreatableType<SelectedScopeModel>("Profiler", 1,0, "SelectedScopeModel", "Cannot create this type");
    qmlRegisterType<ThreadModel>("Profiler", 1,0, "ThreadScopes");
    qmlRegisterType<ScopeModel>("Profiler", 1,0, "ScopeDataModel");
    qmlRegisterType<ScopeTimingsModel>("Profiler", 1,0, "ScopeTimingsModel");

    connect(mCppModels, SIGNAL(openFile(QUrl)), mFileData, SLOT(openProfileFile(QUrl)));
    connect(mFileData, SIGNAL(fileDidLoad(const QString&)), this, SLOT(reactOnFileLoaded(const QString&)));

    mFileData->openProfileFile(":/debug-files/profile2.dat");

    if(mCppModels == nullptr)
        throw std::runtime_error("CppModels must be constructed by now.");

    mQmlEngine->rootContext()->setContextProperty("_cppModels", mCppModels);
    mQmlEngine->load(QUrl(QStringLiteral("qrc:/RootView.qml")));
}


void ProfilerApp::reactOnFileLoaded(const QString &)
{
    FileDataConverter converter(FileDataConverter::HEADER_VERSION_2);
    mRawDataAdapter = converter.convert(mFileData->getData());
    mRawDataAdapter->setParent(this);

    mFrameDataModel = new FrameDataModel(this);
    mFrameDataModel->setRawDataAdapter(mRawDataAdapter);

    mCppModels->setFrame(mFrameDataModel);
    mCppModels->setRawData(mRawDataAdapter);

    mCppModels->setSelectedScope(new SelectedScopeModel(*mFrameDataModel, this));
}
