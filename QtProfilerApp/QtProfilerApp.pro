#-------------------------------------------------
#
# Project created by QtCreator 2015-04-19T19:31:43
#
#-------------------------------------------------
include(../include.pri)

QT       += core gui quick widgets quickwidgets qml

TARGET = QtProfilerApp
TEMPLATE = app

SOURCES += main.cpp \
    Models/CppModels.cpp \
    ProfilerApp.cpp

HEADERS  += \
    macros.h \
    Models/CppModels.h \
    ProfilerApp.h

FORMS    +=

RESOURCES += \
    Reources/qml/qml.qrc \
    Reources/resources/resources.qrc \
    Reources/fakes/fakes.qrc

DISTFILES += \
    readme.txt \
    Reources/qml/ScopeRenderer/qmldir

INCLUDEPATH += $$PWD/../QtProfilerLib
INCLUDEPATH += $$PWD/../QtMeasureLib


win32:CONFIG(release, debug|release): {
#TODO get from libs-qt
    PRE_TARGETDEPS += $$PWD/../libs-qt/measureLib.lib
    PRE_TARGETDEPS += $$PWD/../libs-qt/profilerLib.lib
    LIBS += -L$$PWD/../libs-qt/ -lprofilerLib
    LIBS += -L$$PWD/../libs-qt/ -lmeasureLib
}
else:win32:CONFIG(debug, debug|release): {
#TODO get from libs-qt
    PRE_TARGETDEPS += $$PWD/../libs-qt/measureLibd.lib
    PRE_TARGETDEPS += $$PWD/../libs-qt/profilerLibd.lib
    LIBS += -L$$PWD/../libs-qt/ -lprofilerLibd
    LIBS += -L$$PWD/../libs-qt/ -lmeasureLibd
}
else:unix:CONFIG(release, debug|release): {
    PRE_TARGETDEPS += $$PWD/../libs-qt/libmeasureLib.a
    PRE_TARGETDEPS += $$PWD/../libs-qt/libprofilerLib.a
    LIBS += -L$$PWD/../libs-qt/ -lmeasureLib
    LIBS += -L$$PWD/../libs-qt/ -lprofilerLib
}
else:unix:CONFIG(debug, debug|release): {
    PRE_TARGETDEPS += $$PWD/../libs-qt/libmeasureLibd.a
    PRE_TARGETDEPS += $$PWD/../libs-qt/libprofilerLibd.a
    LIBS += -L$$PWD/../libs-qt/ -lmeasureLibd
    LIBS += -L$$PWD/../libs-qt/ -lprofilerLibd
}


else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../QtProfilerLib/release/QtProfilerLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../QtProfilerLib/debug/QtProfilerLib.lib
