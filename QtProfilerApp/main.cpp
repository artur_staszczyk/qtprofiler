﻿#include "ProfilerApp.h"

#include <QDir>
#include <QDebug>
#include <QGuiApplication>
#include <QQmlApplicationEngine>

#define ENABLE_PROFILING
#include <PerformanceHelper.h>


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine* engine = new QQmlApplicationEngine();
    ProfilerApp profilerApp(engine);

    auto filePath = QDir::currentPath() + "/profile.dat";
    auto storage = new PerformanceHelper::FileOutputStorage(filePath.toStdString());
    qDebug() << "Saving to " << filePath;

    PerformanceHelper::Singleton<PerformanceHelper::MeasurementBuffer>::instance().setStorage(storage);

    MEASURE_SCOPE_START("PrifilerApp load");
    profilerApp.load();
    MEASURE_SCOPE_END();

    return app.exec();
}
