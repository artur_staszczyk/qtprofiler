﻿#ifndef CPPMODELS_H
#define CPPMODELS_H

#include <QUrl>
#include <QObject>
#include <AutoProperty.h>

class IFrameDataModel;
class IRawDataAdapter;
class SelectedScopeModel;

class CppModels : public QObject
{
    Q_OBJECT
public:
    explicit CppModels(QObject *parent = 0);

    AUTO_PROPERTY(IFrameDataModel*, Frame, frame)
    AUTO_PROPERTY(IRawDataAdapter*, RawData, rawData)
    AUTO_PROPERTY(SelectedScopeModel*, SelectedScope, selectedScope)

signals:
    Q_INVOKABLE void openFile(QUrl fileName);

public slots:


};

#endif // CPPMODELS_H
