﻿#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <Helpers.h>

int main(int argc, char *argv[])
{
    star_help::Random::Initialize();
    ::testing::InitGoogleMock(&argc, argv);
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
