﻿#include <gtest/gtest.h>

#include <QString>

#include <Models/RawDataAdapter.h>

#include "Helpers/TestBufferBuilder.h"

QByteArray testRawData              ("test");
QVector<quint32> testFramesOffsets =    { 0, 4 };
QVector<quint32> testFramesScopeCount = { 1, 3 };
QVector<QString> testScopes =       { "test1", "test2" };
QVector<double> testTimings =       { 10, 20 };

TEST(RawDataAdapterFixture, SetRawData)
{
    // given
    RawDataAdapter rawData;
    rawData.setRawMeasurementData(testRawData);

    // expect
    QString expected(testRawData);
    QString actual(rawData.getRawMeasurementsData());

    ASSERT_EQ(actual.toStdString(), expected.toStdString());
}

TEST(RawDataAdapterFixture, FramesCalculations)
{
    // given
    RawDataAdapter rawData;
    rawData.setPerFrameDataOffsets(testFramesOffsets).
            setPerFrameScopesCount(testFramesScopeCount);

    // assert
    ASSERT_EQ(rawData.getFramesCount(), 2u);

    EXPECT_EQ(rawData.getDataOffsetForFrame(0), testFramesOffsets[0]);
    EXPECT_EQ(rawData.getDataOffsetForFrame(1), testFramesOffsets[1]);

    EXPECT_EQ(rawData.getMeasurementCountForFrame(0), testFramesScopeCount[0]);
    EXPECT_EQ(rawData.getMeasurementCountForFrame(1), testFramesScopeCount[1]);
}

TEST(RawDataAdapterFixture, FrameTimingsCalculations)
{
    // given
    RawDataAdapter rawData;
    rawData.setPerFrameTimings(testTimings);

    // assert
    EXPECT_EQ(rawData.getFrameDuration(0), 10);
    EXPECT_EQ(rawData.getFrameDuration(1), 20);

    EXPECT_EQ(rawData.maxFrameDuration(), 20);
}

TEST(RawDataAdapterFixture, ScopesOperations)
{
    // given
    RawDataAdapter rawData;
    rawData.setGlobalScopeNames(testScopes);

    // assert
    ASSERT_EQ(rawData.getScopeNamesCount(), 2u);

    EXPECT_EQ(rawData.getScopeNameForIndex(0), testScopes[0]);
    EXPECT_EQ(rawData.getScopeNameForIndex(1), testScopes[1]);
}

TEST(RawDataAdapterFixture, ThrowOnBadScopeIndex)
{
    // given
    RawDataAdapter rawData;
    rawData.setGlobalScopeNames(testScopes);

    // assert
    EXPECT_THROW(rawData.getScopeNameForIndex(3), std::out_of_range);
    EXPECT_THROW(rawData.getScopeNameForIndex(2), std::out_of_range);
    EXPECT_THROW(rawData.getScopeNameForIndex(-1), std::out_of_range);
}

TEST(RawDataAdapterFixture, ThrowOnBadTimingIndex)
{
    // given
    RawDataAdapter rawData;
    rawData.setPerFrameTimings(testTimings);

    // assert
    EXPECT_THROW(rawData.getFrameDuration(3), std::out_of_range);
    EXPECT_THROW(rawData.getFrameDuration(2), std::out_of_range);
    EXPECT_THROW(rawData.getFrameDuration(-1), std::out_of_range);
}

TEST(RawDataAdapterFixture, PerMeasurementCall)
{
    RawDataAdapter rawData;
    rawData.setPerFrameDataOffsets(testFramesOffsets).
            setPerFrameScopesCount(testFramesScopeCount);

    TestBufferBuilder builder;
    builder.addValues<quint64>({ 1, 2, 3, 4});

    double frameTimes[6] = { 5.0, 6.0, 7.0, 8.0, 9.0, 10.0 };
    quint64 threadIds[3] = { 7767, 7768, 7769 };
    quint64 scopeNames[3] = { 1, 2, 3 };

    for(int i = 0; i < 3; ++i)
    {
        builder.addValues<double>({ frameTimes[i * 2], frameTimes[i * 2 + 1] });
        builder.addValue(static_cast<quint64>(threadIds[i] | scopeNames[i] << 32));
    }

    rawData.setRawMeasurementData(builder.build());

    int timesExecuted = 0;
    rawData.readFrameMeasurements(2, [&](double start, double end, quint32 thread, quint32 scope){
       EXPECT_EQ(frameTimes[timesExecuted * 2], start);
       EXPECT_EQ(frameTimes[timesExecuted * 2 + 1], end);
       EXPECT_EQ(threadIds[timesExecuted], thread);
       EXPECT_EQ(scopeNames[timesExecuted], scope);

       timesExecuted++;
    });
}
