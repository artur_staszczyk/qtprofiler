#-------------------------------------------------
#
# Project created by QtCreator 2015-09-24T20:21:12
#
#-------------------------------------------------
include(../../include.pri)

TEMPLATE = app
TARGET = ProfilerLibTests

CONFIG   += console
CONFIG   -= app_bundle

QT += core testlib qml widgets
QT -= gui

INCLUDEPATH += $$PWD/../../QtProfilerLib
INCLUDEPATH += $$PWD/../../include/MeasureLib


win32:CONFIG(release, debug|release): {
    #TODO get from libs-qt
    PRE_TARGETDEPS += $$OUT_PWD/../../QtProfilerLib/release/QtProfilerLib.lib
    #TODO LIBS
}
else:win32:CONFIG(debug, debug|release): {
    #TODO get from libs-qt
    PRE_TARGETDEPS += $$OUT_PWD/../../QtProfilerLib/debug/QtProfilerLibd.lib
    #TODO LIBS
}
else:unix:CONFIG(release, debug|release): {
    PRE_TARGETDEPS += $$PWD/../../libs-qt/libprofilerLib.a
    PRE_TARGETDEPS += $$PWD/../../libs-qt/libmeasureLib.a
    LIBS += -L$$PWD/../../libs-qt -lprofilerLib -lmeasureLib
}
else:unix:CONFIG(debug, debug|release): {
    PRE_TARGETDEPS += $$PWD/../../libs-qt/libprofilerLibd.a
    PRE_TARGETDEPS += $$PWD/../../libs-qt/libmeasureLibd.a
    LIBS += -L$$PWD/../../libs-qt -lprofilerLibd -lmeasureLibd
}

SOURCES += main.cpp \
    RawDataAdapterTest.cpp \
    FrameDataModelTest.cpp \
    FileDataConverterTest.cpp \
    SelectedScopeModelTest.cpp \
    ThreadModelTest.cpp \
    ScopeTimingsModelTest.cpp \
    ScopeModelTest.cpp

RESOURCES += \
    TestingFiles.qrc

DISTFILES += \
    BadVersionRaw.dat \
    SimpleFile.dat

HEADERS += \
    Mocks/RawDataAdapterMock.h \
    Helpers/TestBufferBuilder.h \
    Mocks/FrameDataModelMock.h \
    Helpers/ScopeTimingsGenerator.h \
    Helpers/ScopesGenerator.h \
    Helpers/ThreadGenerator.h
