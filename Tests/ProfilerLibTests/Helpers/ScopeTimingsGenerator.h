﻿#ifndef SCOPETIMINGSGENERATOR_H
#define SCOPETIMINGSGENERATOR_H

#include "Helpers.h"
#include <Models/ScopeTimingsModel.h>

inline ScopeTimingsModel* generateScopeTiming(QObject* parent)
{
    auto scopeTiming = new ScopeTimingsModel(parent);

    scopeTiming->setIsGroup((bool)star_help::Random::getInt(0, 1));
    scopeTiming->setAverage(star_help::Random::nextReal<double>());
    scopeTiming->setCount(star_help::Random::nextInt<uint32_t>());
    scopeTiming->setMaxDistance(star_help::Random::nextReal<double>());
    scopeTiming->setStart(star_help::Random::getReal(0.0, 0.1));
    scopeTiming->setStop(star_help::Random::getReal(1.0, 2.0));

    return scopeTiming;
}

#endif // SCOPETIMINGSGENERATOR_H
