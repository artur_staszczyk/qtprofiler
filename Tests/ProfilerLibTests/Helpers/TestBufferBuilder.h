﻿#ifndef FILECONVERTETTESTHELPER_H
#define FILECONVERTETTESTHELPER_H

#include <QByteArray>
#include <QDataStream>

class TestBufferBuilder {

public:

    TestBufferBuilder()
        : mDataStream(&mBuffer, QIODevice::ReadWrite)
    {
        mDataStream.setByteOrder(QDataStream::LittleEndian);
    }

    QByteArray build() const { return mBuffer; }

    TestBufferBuilder& addValue(quint64 value)
    {
        mDataStream << value;
        return *this;
    }

    template
    <typename T>
    TestBufferBuilder& addValues(std::vector<T> data)
    {
        QByteArray array;
        QDataStream stream(&array, QIODevice::WriteOnly);

        std::for_each(data.begin(), data.end(), [&](const T& element){
            mDataStream << reinterpret_cast<const quint64&>(element);
        });

        return *this;
    }

    TestBufferBuilder& addString(const std::string& value)
    {
        mDataStream << static_cast<quint8>(value.length());
        std::for_each(value.begin(), value.end(), [&](char element){
            mDataStream << static_cast<quint8>(element);
        });

        return *this;
    }

private:

    QByteArray mBuffer;
    QDataStream mDataStream;

};

#endif // FILECONVERTETTESTHELPER_H
