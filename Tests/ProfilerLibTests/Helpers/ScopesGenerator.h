﻿#ifndef SCOPESGENERATOR_H
#define SCOPESGENERATOR_H

inline ScopeModel* generateScope(QObject* parent, const std::string& name, QList<ScopeTimingsModel*> timings = {})
{
    auto scope = new ScopeModel(parent);
    scope->setItemName(name.c_str());
    scope->setTimings(timings);
    return scope;
}

#endif // SCOPESGENERATOR_H
