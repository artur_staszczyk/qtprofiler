﻿#ifndef THREADGENERATOR_H
#define THREADGENERATOR_H

#include <QList>

#include <Models/ScopeModel.h>
#include <Models/ThreadModel.h>

inline ThreadModel* generateThread(QObject* parent, const std::string& name, QList<ScopeModel*> scopes = {})
{
    auto thread = new ThreadModel(parent);
    thread->setThreadId(name.c_str());
    thread->setScopes(scopes);

    return thread;
}

#endif // THREADGENERATOR_H
