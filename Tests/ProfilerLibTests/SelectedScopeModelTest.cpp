﻿#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <Models/ScopeTimingsModel.h>
#include <Models/SelectedScopeModel.h>

#include "Mocks/FrameDataModelMock.h"

#include "Helpers/ScopesGenerator.h"
#include "Helpers/ScopeTimingsGenerator.h"

class SelectedScopeModelFixture : public ::testing::Test
{

    void SetUp() override
    {
        auto timing1 = generateScopeTiming(&mTestThreadModel);
        auto timing2 = generateScopeTiming(&mTestThreadModel);
        auto timing3 = generateScopeTiming(&mTestThreadModel);
        auto timing4 = generateScopeTiming(&mTestThreadModel);

        auto scope1 = generateScope(&mTestThreadModel, "scope1", {timing1, timing2});
        auto scope2 = generateScope(&mTestThreadModel, "scope2", {timing3, timing4});


        mTestThreadModel.setThreadId("test1");
        mTestThreadModel.setScopes({scope1, scope2});

        auto retTuple = std::tuple<QString, QString, ScopeTimingsModel*>("thread1", "scope1", timing1);
        ON_CALL(mFrameDataModel, findTimingWithId(quint64(timing1->getUniqueId()))).WillByDefault(::testing::Return(retTuple));

        mSelectedTiming = reinterpret_cast<quint64>(timing1);
        mExpectedDuration = timing1->getStop() - timing1->getStart();
    }

public:
    ThreadModel mTestThreadModel;
    ::testing::NiceMock<FrameDataModelMock> mFrameDataModel;

    quint64 mSelectedTiming;
    double mExpectedDuration;
};

TEST_F(SelectedScopeModelFixture, ReactOnChangedScope)
{
    SelectedScopeModel selectedScope(mFrameDataModel);

    selectedScope.setSelectedTimingId(mSelectedTiming);

    EXPECT_EQ(selectedScope.getAnyScopeSelected(), true);
    EXPECT_EQ(selectedScope.getThreadId().toStdString(), "thread1");
    EXPECT_EQ(selectedScope.getSelectedScopeName().toStdString(), "scope1");
    EXPECT_NEAR(selectedScope.getDuration(), mExpectedDuration, std::numeric_limits<double>::epsilon());
}
