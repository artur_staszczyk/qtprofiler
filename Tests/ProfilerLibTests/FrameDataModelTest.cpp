﻿#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <QSignalSpy>
#include <QPointF>

#include <Models/FrameDataModel.h>
#include "Mocks/RawDataAdapterMock.h"

#include "Helpers/ThreadGenerator.h"
#include "Helpers/ScopesGenerator.h"
#include "Helpers/ScopeTimingsGenerator.h"

class FrameDataModelFixture : public ::testing::Test
{
public:
    void SetUp() override
    {
        mFrameDataModel.setRawDataAdapter(&mRawDataAdapter);
        ON_CALL(mRawDataAdapter, readFrameMeasurements(::testing::_, ::testing::_)).
                WillByDefault(
                    ::testing::Invoke(this, &FrameDataModelFixture::delegatedReadFrameMeasurements));

        ON_CALL(mRawDataAdapter, getScopeNameForIndex(0)).WillByDefault(::testing::Return("test1"));
        ON_CALL(mRawDataAdapter, getScopeNameForIndex(1)).WillByDefault(::testing::Return("test2"));

        ON_CALL(mRawDataAdapter, getFramesCount()).WillByDefault(::testing::Return(3));
    }

public:
    FrameDataModel mFrameDataModel;
    ::testing::NiceMock<RawDataAdapterMock> mRawDataAdapter;

    ThreadModel* buildTestThread(const std::string& threadName)
    {
        ScopeTimingsModel *t1, *t2, *t3, *t4;
        t1 = generateScopeTiming(&mFrameDataModel);
        t2 = generateScopeTiming(&mFrameDataModel);
        t3 = generateScopeTiming(&mFrameDataModel);
        t4 = generateScopeTiming(&mFrameDataModel);

        ScopeModel *s1, *s2;
        s1 = generateScope(&mFrameDataModel, "scope1", {t1, t2});
        s2 = generateScope(&mFrameDataModel, "scope2", {t3, t4});

        auto* thread = generateThread(&mFrameDataModel, threadName, {s1, s2});

        return thread;
    }

private:
    void delegatedReadFrameMeasurements(quint32 frameId, std::function< void (double, double, quint32, quint32)> function)
    {
        for(int i = 0; i < 2; ++i)
            function((2 * frameId) + i, (2 * frameId) + i + 0.5, 7767, i % 2);
    }
};

TEST_F(FrameDataModelFixture, NotifyOnActiveFrameChange)
{
    // given
    QSignalSpy spyForStartTime(&mFrameDataModel, SIGNAL(frameStartTimeChanged(double)));
    QSignalSpy spyForEndTime(&mFrameDataModel, SIGNAL(frameEndTimeChanged(double)));

    // try
    mFrameDataModel.setActiveFrame(2);

    // assert
    EXPECT_EQ(1, spyForStartTime.count());
    EXPECT_EQ(1, spyForEndTime.count());
}

TEST_F(FrameDataModelFixture, DontNotifyOnFrameNotChanged)
{
    // given
    mFrameDataModel.setActiveFrame(1);
    QSignalSpy spy(&mFrameDataModel, SIGNAL(activeFrameChanged(quint32)));

    // try
    mFrameDataModel.setActiveFrame(1);

    // assert
    EXPECT_EQ(0, spy.count());
}

TEST_F(FrameDataModelFixture, ThrowingIfBadFrameWasSet)
{
    // assert
    EXPECT_THROW(mFrameDataModel.setActiveFrame(5), std::invalid_argument);
    EXPECT_THROW(mFrameDataModel.setActiveFrame(0), std::invalid_argument);
}

TEST_F(FrameDataModelFixture, CalculatingFrameStartAndEnd)
{
    mFrameDataModel.setActiveFrame(2);

    EXPECT_NEAR(4, mFrameDataModel.getFrameStartTime(), std::numeric_limits<double>::epsilon());
    EXPECT_NEAR(5.5, mFrameDataModel.getFrameEndTime(), std::numeric_limits<double>::epsilon());
}

TEST_F(FrameDataModelFixture, GetThreadList)
{
    // given
    mFrameDataModel.setActiveFrame(2);

    // try
    QList<ThreadModel*> threads = mFrameDataModel.getThreadsRaw();

    EXPECT_EQ(threads.size(), 1);
    EXPECT_EQ(threads[0]->getThreadId(), QString("Thread 7767"));
}

TEST_F(FrameDataModelFixture, GetScopeNames)
{
    mFrameDataModel.setActiveFrame(2);

    // try
    QList<ThreadModel*> threads = mFrameDataModel.getThreadsRaw();

    EXPECT_EQ(threads[0]->getScopesRaw().size(), 2);

    EXPECT_EQ(threads[0]->getScopesRaw()[0]->getItemName(), QString("test1"));
    EXPECT_EQ(threads[0]->getScopesRaw()[1]->getItemName(), QString("test2"));
}

TEST_F(FrameDataModelFixture, GetScopeTimings)
{
    mFrameDataModel.setActiveFrame(2);

    // try
    QList<ThreadModel*> threads = mFrameDataModel.getThreadsRaw();

    EXPECT_EQ(threads[0]->getScopesRaw()[0]->getTimingsRaw().size(), 1);
    EXPECT_NEAR(threads[0]->getScopesRaw()[0]->getTimingsRaw()[0]->getStart(), 4.0, std::numeric_limits<double>::epsilon());
    EXPECT_NEAR(threads[0]->getScopesRaw()[0]->getTimingsRaw()[0]->getStop(), 4.5, std::numeric_limits<double>::epsilon());
    EXPECT_NEAR(threads[0]->getScopesRaw()[0]->getTimingsRaw()[0]->getAverage(), 0.0, std::numeric_limits<double>::epsilon());
}

TEST_F(FrameDataModelFixture, TestFindingThread)
{
    auto thread1 = std::unique_ptr<ThreadModel>(buildTestThread("t1"));
    auto thread2 = std::unique_ptr<ThreadModel>(buildTestThread("t2"));
    mFrameDataModel.addThreads(thread1.get());
    mFrameDataModel.addThreads(thread2.get());

    auto found = mFrameDataModel.findThreadNamed("t1");
    EXPECT_EQ(found->getThreadId(), "t1");

    found = mFrameDataModel.findThreadNamed("t2");
    EXPECT_EQ(found->getThreadId(), "t2");
}

TEST_F(FrameDataModelFixture, TestThrowOnInvalidThread)
{
    EXPECT_THROW(mFrameDataModel.findThreadNamed("t3"), std::invalid_argument);
}

TEST_F(FrameDataModelFixture, TestFindTimings)
{
    auto threadModel = std::unique_ptr<ThreadModel>(buildTestThread("thread1"));
    mFrameDataModel.addThreads(threadModel.get());

    auto timing = generateScopeTiming(&mFrameDataModel);
    auto scope = generateScope(&mFrameDataModel, "scope1", {timing});
    auto thread = generateThread(&mFrameDataModel, "thread1", {scope});

    mFrameDataModel.addThreads(thread);

    auto tuple = mFrameDataModel.findTimingWithId(timing->getUniqueId());

    EXPECT_EQ(std::get<0>(tuple), "thread1");
    EXPECT_EQ(std::get<1>(tuple), "scope1");
    EXPECT_EQ(std::get<2>(tuple), timing);
}
