﻿#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <Models/ThreadModel.h>
#include <Models/ScopeModel.h>

TEST(ThreadModelFixture, TestFindingScope)
{
    ThreadModel threadScopes;

    ScopeModel s1, s2;
    s1.setItemName("scope1");
    s2.setItemName("scope2");

    threadScopes.addScopes(&s1);
    threadScopes.addScopes(&s2);

    EXPECT_EQ(threadScopes.findScopeNamed("scope1"), &s1);
    EXPECT_EQ(threadScopes.findScopeNamed("scope2"), &s2);
}

TEST(ThreadModelFixture, TestThrowOnInvalidScope)
{
    ThreadModel threadScopes;
    EXPECT_THROW(threadScopes.findScopeNamed("scope3"), std::invalid_argument);
}

