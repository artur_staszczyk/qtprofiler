﻿#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <Exceptions.h>
#include <Measurement.h>

#include <Models/RawDataAdapter.h>
#include <FileLoading/FileDataConverter.h>

#include "Helpers/TestBufferBuilder.h"

using namespace PerformanceHelper;

static quint64 thread1 = 1;
static quint64 thread2 = 2;

static quint64 functionName1 = 0;
static quint64 functionName2 = 1;
static quint64 functionName3 = 2;

using namespace std;

static QByteArray testProfileFile()
{
    return TestBufferBuilder().addValue(FileDataConverter::HEADER_VERSION_2).
        addValue(ENTRY_SIZE_IN_BYTES * 4 + FRAME_END_SIZE_IN_BYTES * 2).

        addValues<double>({1.0, 2.0}).
        addValue(static_cast<quint64>(thread1 | functionName1 << 32)).

        addValues<double>({2.0, 3.0}).
        addValue(static_cast<quint64>(thread2 | functionName2 << 32)).

        addValue(FileDataConverter::FRAME_END_MARKER).

        addValues<double>({4.0, 5.0}).
        addValue(static_cast<quint64>(thread1 | functionName3 << 32)).

        addValues<double>({3.0, 6.0}).
        addValue(static_cast<quint64>(thread2 | functionName1 << 32)).

        addValue(FileDataConverter::FRAME_END_MARKER).

        addValue(3).
        addString("test1").
        addString("test2").
        addString("test3").
        build();
}

TEST(FileDataConverterTest, throwingOnBadHeader)
{
    auto badHeaderData = TestBufferBuilder().addValue(2).build();

    FileDataConverter converter(FileDataConverter::HEADER_VERSION_2);

    ASSERT_THROW(converter.convert(badHeaderData), bad_version);
}

TEST(FileDataConverterTest, throwingOnInvalidData)
{
    auto badData = TestBufferBuilder().addValues<quint64>({FileDataConverter::HEADER_VERSION_2, 3}).build();

    FileDataConverter converter(FileDataConverter::HEADER_VERSION_2);

    ASSERT_THROW(converter.convert(badData), std::out_of_range);
}

TEST(FileDataConverterTest, testSimpleFileMetaData)
{
    auto testBuffer = testProfileFile();

    FileDataConverter converter(FileDataConverter::HEADER_VERSION_2);
    auto adapter = converter.convert(testBuffer);

    ASSERT_NE(adapter, nullptr);
    ASSERT_EQ(typeid(*adapter), typeid(RawDataAdapter));
    ASSERT_EQ(adapter->getScopeNamesCount(), 3);

    ASSERT_EQ(adapter->getScopeNameForIndex(functionName1), "test1");
    ASSERT_EQ(adapter->getScopeNameForIndex(functionName2), "test2");
    ASSERT_EQ(adapter->getScopeNameForIndex(functionName3), "test3");

    ASSERT_EQ(adapter->getFramesCount(), 2);
    ASSERT_EQ(adapter->getDataOffsetForFrame(0), 0);
    ASSERT_EQ(adapter->getDataOffsetForFrame(1), 7);

    ASSERT_EQ(adapter->getMeasurementCountForFrame(0), 2);
    ASSERT_EQ(adapter->getMeasurementCountForFrame(1), 2);

    ASSERT_EQ(adapter->getFrameDuration(0), 2);
    ASSERT_EQ(adapter->getFrameDuration(1), 3);

    ASSERT_EQ(adapter->maxFrameDuration(), 3);
}

TEST(FileDataConverterTest, testSimpleProfileData)
{
    auto testBuffer = testProfileFile();

    FileDataConverter converter(FileDataConverter::HEADER_VERSION_2);
    auto adapter = converter.convert(testBuffer);

    ASSERT_NE(adapter, nullptr);

    auto rawByteArray = adapter->getRawMeasurementsData();

    const quint64* bufferLongPtr;
    const quint32* bufferPtr;

    bufferLongPtr = reinterpret_cast<const quint64*>(rawByteArray.constData());

    // FIRST ENTRY
    ASSERT_DOUBLE_EQ(reinterpret_cast<const double&>(*bufferLongPtr), 1.0);
    bufferLongPtr++;
    ASSERT_DOUBLE_EQ(reinterpret_cast<const double&>(*bufferLongPtr), 2.0);
    bufferLongPtr++;

    bufferPtr = reinterpret_cast<const quint32*>(bufferLongPtr);
    ASSERT_EQ(*bufferPtr, thread1);
    bufferPtr++;
    ASSERT_EQ(*bufferPtr, 0);
    bufferLongPtr++;

    // SECOND ENTRY
    ASSERT_DOUBLE_EQ(reinterpret_cast<const double&>(*bufferLongPtr), 2.0);
    bufferLongPtr++;
    ASSERT_DOUBLE_EQ(reinterpret_cast<const double&>(*bufferLongPtr), 3.0);
    bufferLongPtr++;

    bufferPtr = reinterpret_cast<const quint32*>(bufferLongPtr);
    ASSERT_EQ(*bufferPtr, thread2);
    bufferPtr++;
    ASSERT_EQ(*bufferPtr, 1);
    bufferLongPtr++;

    // FRAME END
    ASSERT_EQ(*bufferLongPtr, FileDataConverter::FRAME_END_MARKER);
    bufferLongPtr++;

    // THIRD ENTRY
    ASSERT_DOUBLE_EQ(reinterpret_cast<const double&>(*bufferLongPtr), 4.0);
    bufferLongPtr++;
    ASSERT_DOUBLE_EQ(reinterpret_cast<const double&>(*bufferLongPtr), 5.0);
    bufferLongPtr++;

    bufferPtr = reinterpret_cast<const quint32*>(bufferLongPtr);
    ASSERT_EQ(*bufferPtr, thread1);
    bufferPtr++;
    ASSERT_EQ(*bufferPtr, functionName3);
    bufferLongPtr++;

    // FOURTH ENTRY
    ASSERT_DOUBLE_EQ(reinterpret_cast<const double&>(*bufferLongPtr), 3.0);
    bufferLongPtr++;
    ASSERT_DOUBLE_EQ(reinterpret_cast<const double&>(*bufferLongPtr), 6.0);
    bufferLongPtr++;

    bufferPtr = reinterpret_cast<const quint32*>(bufferLongPtr);
    ASSERT_EQ(*bufferPtr, thread2);
    bufferPtr++;
    ASSERT_EQ(*bufferPtr, functionName1);
    bufferLongPtr++;

    // FRAME END
    ASSERT_EQ(*bufferLongPtr, FileDataConverter::FRAME_END_MARKER);
    bufferLongPtr++;
}
