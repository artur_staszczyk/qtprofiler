﻿#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <Helpers.h>
#include <Models/ScopeModel.h>
#include <Models/ScopeTimingsModel.h>

#include "Helpers/ScopeTimingsGenerator.h"

using namespace star_help;

TEST(ScopeModelFixture, TestFindingTimings)
{
    ScopeModel scope;

    auto timings1 = generateScopeTiming(&scope);
    auto timings2 = generateScopeTiming(&scope);
    auto timings3 = generateScopeTiming(&scope);

    scope.addTimings(timings1);
    scope.addTimings(timings2);
    scope.addTimings(timings3);

    auto foundTimings = scope.findTimings(timings1->getUniqueId());

    EXPECT_EQ(foundTimings, timings1);
}

TEST(ScopeModelFixture, TestThrowOnInvalidTiming)
{
    ScopeModel scope;

    EXPECT_THROW(scope.findTimings(1), std::invalid_argument);
}
