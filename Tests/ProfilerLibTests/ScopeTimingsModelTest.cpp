﻿#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <Models/ScopeTimingsModel.h>

TEST(ScopeTimingsModelFixture, TestUniqueId)
{
    ScopeTimingsModel scopeTimings;

    EXPECT_EQ(scopeTimings.getUniqueId(), (quint64)&scopeTimings);
}
