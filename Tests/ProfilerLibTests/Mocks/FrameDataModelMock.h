﻿#ifndef FRAMEDATAMODELMOCK_H
#define FRAMEDATAMODELMOCK_H

#include <gmock/gmock.h>
#include <QDataStream>

#include <Models/IFrameDataModel.h>

class FrameDataModelMock : public IFrameDataModel
{
public:
    MOCK_CONST_METHOD1(findThreadNamed, ThreadModel*(const QString&));
    MOCK_CONST_METHOD1(findTimingWithId, std::tuple<QString, QString, ScopeTimingsModel*>(quint64));
    MOCK_METHOD1(setRawDataAdapter, void(IRawDataAdapter*));

    MOCK_CONST_METHOD0(getActiveFrame, quint32());
};

#endif // FRAMEDATAMODELMOCK_H
