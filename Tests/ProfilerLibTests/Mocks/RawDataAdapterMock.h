﻿#ifndef RAWDATAADAPTERMOCK_H
#define RAWDATAADAPTERMOCK_H

#include <gmock/gmock.h>
#include <QDataStream>

#include <Models/IRawDataAdapter.h>

class RawDataAdapterMock : public IRawDataAdapter
{
public:

    MOCK_CONST_METHOD0(getRawMeasurementsData, QByteArray());
    MOCK_CONST_METHOD0(getFramesCount, quint32());

    MOCK_CONST_METHOD1(getScopeNameForIndex, QString(quint32));

    MOCK_METHOD2(readFrameMeasurements, void (quint32, std::function< void (double, double, quint32, quint32)>));

    MOCK_CONST_METHOD1(getFrameDuration, double (quint32));
    MOCK_CONST_METHOD1(getDataOffsetForFrame, quint32 (quint32));
    MOCK_CONST_METHOD1(getMeasurementCountForFrame, quint32 (quint32));
    MOCK_CONST_METHOD0(getScopeNamesCount, quint32 ());
    MOCK_CONST_METHOD0(getThreadCount, quint32 ());
    MOCK_CONST_METHOD1(getThreadName, quint32(quint32));
    MOCK_CONST_METHOD0(maxFrameDuration, double());
};

#endif // RAWDATAADAPTERMOCK_H

