﻿#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <QSignalSpy>

#include <Exceptions.h>
#include <FileLoading/FileData.h>
#include <FileLoading/FileDataConverter.h>
#include <Models/RawDataAdapter.h>

QString emptyFile = ":/version2/DataVersion2/EmptyFile.dat";
QString fileVersion1 = ":/version1/DataVersion1/Profile1.dat";
QString fileVersion2ST = ":/version2/DataVersion2/SingleThreaded.dat";

class FileDataSTFixture : public testing::Test
{
public:
    FileData mFileData;
    QSignalSpy mSpy;

    FileDataSTFixture()
        : mSpy(&mFileData, SIGNAL(fileDidLoad(QString)))
    {
    }

    std::unique_ptr<IRawDataAdapter> adapterFromData()
    {
        auto converter = std::make_unique<FileDataConverter>(FileDataConverter::HEADER_VERSION_2);
        auto rawDataAdapter = converter->convert(mFileData.getData());
        return std::unique_ptr<IRawDataAdapter>(rawDataAdapter);
    }
};

TEST_F(FileDataSTFixture, ThrowingOnEmptyFile)
{
    ASSERT_THROW(mFileData.openProfileFile(emptyFile), std::runtime_error);
    ASSERT_EQ(mSpy.count(), 0);
}

TEST_F(FileDataSTFixture, ThrowingOnNonExistingFileFile)
{
    ASSERT_THROW(mFileData.openProfileFile("non-existing"), std::runtime_error);
    ASSERT_EQ(mSpy.count(), 0);
}

TEST_F(FileDataSTFixture, LoadsData)
{
    mFileData.openProfileFile(fileVersion2ST);

    EXPECT_EQ(mFileData.getData().size(), 192);
    EXPECT_EQ(mFileData.getHeader(), FileDataConverter::HEADER_VERSION_2);
    EXPECT_EQ(mSpy.count(), 1);
}

TEST_F(FileDataSTFixture, CanConvertToAdapter)
{
    mFileData.openProfileFile(fileVersion2ST);
    EXPECT_EQ(mSpy.count(), 1);

    auto adapter = adapterFromData();

    EXPECT_NE(adapter.get(), nullptr);
    EXPECT_EQ(typeid(RawDataAdapter), typeid(*adapter));

    EXPECT_EQ(adapter->getFramesCount(), 2);
    EXPECT_EQ(adapter->getScopeNamesCount(), 2);
}

TEST_F(FileDataSTFixture, OpeningSecondFile)
{
    mFileData.openProfileFile(fileVersion2ST);
    mFileData.openProfileFile(fileVersion2ST);

    EXPECT_EQ(mFileData.getData().size(), 192);
    EXPECT_EQ(mFileData.getHeader(), FileDataConverter::HEADER_VERSION_2);
    EXPECT_EQ(2, mSpy.count());
}

TEST_F(FileDataSTFixture, ThrowOnBadFile)
{
    mFileData.openProfileFile(fileVersion1);
    auto converter = std::make_unique<FileDataConverter>(FileDataConverter::HEADER_VERSION_2);

    EXPECT_THROW(converter->convert(mFileData.getData()), bad_version);
}

TEST_F(FileDataSTFixture, CorrectFrameData)
{
    mFileData.openProfileFile(fileVersion2ST);

    auto rawDataAdapter = adapterFromData();

    ASSERT_EQ(2, rawDataAdapter->getFramesCount());
    ASSERT_DOUBLE_EQ(2.2929120000000003, rawDataAdapter->maxFrameDuration());

    double expectedFramesDuration[] = {0.51732499999999992, 2.2929120000000003};
    for(auto frameId = 0u; frameId < rawDataAdapter->getFramesCount(); ++frameId)
    {
        ASSERT_DOUBLE_EQ(expectedFramesDuration[frameId], rawDataAdapter->getFrameDuration(frameId));
    }

    quint64 expectedOffsets[] = {0, 7};
    for(auto frameId = 0u; frameId < rawDataAdapter->getFramesCount(); ++frameId)
    {
        ASSERT_EQ(expectedOffsets[frameId], rawDataAdapter->getDataOffsetForFrame(frameId));
    }

    quint32 expectedMeasurementCount[] = {2, 4};
    for(auto frameId = 0u; frameId < rawDataAdapter->getFramesCount(); ++frameId)
    {
        ASSERT_EQ(expectedMeasurementCount[frameId], rawDataAdapter->getMeasurementCountForFrame(frameId));
    }
}

TEST_F(FileDataSTFixture, CorrectScopeData)
{
    mFileData.openProfileFile(fileVersion2ST);

    auto rawDataAdapter = adapterFromData();

    ASSERT_EQ(2, rawDataAdapter->getScopeNamesCount());

    std::string expectedNames[] = {"divide", "multiply"};
    for(auto scopeId = 0u; scopeId < rawDataAdapter->getScopeNamesCount(); ++scopeId)
    {
        ASSERT_EQ(expectedNames[scopeId], rawDataAdapter->getScopeNameForIndex(scopeId).toStdString());
    }
}

TEST_F(FileDataSTFixture, CorrectThreadData)
{
    mFileData.openProfileFile(fileVersion2ST);

    auto rawDataAdapter = adapterFromData();

    ASSERT_EQ(1, rawDataAdapter->getThreadCount());
    quint32 expectedThreadIds[] = {4204607414};
    for(auto thread = 0u; thread < rawDataAdapter->getThreadCount(); ++thread)
    {
        ASSERT_EQ(expectedThreadIds[thread], rawDataAdapter->getThreadName(thread));
    }
}
