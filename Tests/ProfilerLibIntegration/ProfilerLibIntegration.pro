#-------------------------------------------------
#
# Project created by QtCreator 2015-10-03T16:18:34
#
#-------------------------------------------------
include(../../include.pri)

TEMPLATE = app
TARGET = ProfilerLibIntegration

CONFIG   += console
CONFIG   -= app_bundle

QT += core testlib qml widgets
QT -= gui

INCLUDEPATH += $$PWD/../../QtProfilerLib

win32:CONFIG(release, debug|release): {
    #TODO get from libs-qt
    PRE_TARGETDEPS += $$OUT_PWD/../../QtProfilerLib/release/QtProfilerLib.lib
    #TODO LIBS
}
else:win32:CONFIG(debug, debug|release): {
    #TODO get from libs-qt
    PRE_TARGETDEPS += $$OUT_PWD/../../QtProfilerLib/debug/QtProfilerLibd.lib
    #TODO LIBS
}
else:unix:CONFIG(release, debug|release): {
    PRE_TARGETDEPS += $$PWD/../../libs-qt/libprofilerLib.a
    LIBS += -L$$PWD/../../libs-qt -lprofilerLib
}
else:unix:CONFIG(debug, debug|release): {
    PRE_TARGETDEPS += $$PWD/../../libs-qt/libprofilerLibd.a
    LIBS += -L$$PWD/../../libs-qt -lprofilerLibd
}

SOURCES += main.cpp \
    FileDataTest.cpp \
    FileDataMultiThreadTest.cpp

RESOURCES += \
    TestingFiles.qrc
