﻿#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <FileLoading/FileData.h>
#include <FileLoading/FileLoader.h>
#include <FileLoading/FileDataConverter.h>

#include <QSignalSpy>

QString fileVersion2MT = ":/version2/DataVersion2/MultiThreaded.dat";

class FileDataMTFixture : public testing::Test
{
public:
    FileData mFileData;
    QSignalSpy mSpy;

    FileDataMTFixture()
        : mSpy(&mFileData, SIGNAL(fileDidLoad(QString)))
    {
    }

    std::unique_ptr<IRawDataAdapter> adapterFromData()
    {
        auto converter = std::make_unique<FileDataConverter>(FileDataConverter::HEADER_VERSION_2);
        auto rawDataAdapter = converter->convert(mFileData.getData());
        return std::unique_ptr<IRawDataAdapter>(rawDataAdapter);
    }
};

TEST_F(FileDataMTFixture, MultiThreadedFile)
{
    mFileData.openProfileFile(fileVersion2MT);

    auto rawDataAdapter = adapterFromData();

    ASSERT_EQ(2, rawDataAdapter->getThreadCount());
    quint32 expectedThreadIds[] = {4204607414, 1997982672};
    for(auto thread = 0u; thread < rawDataAdapter->getThreadCount(); ++thread)
    {
        ASSERT_EQ(expectedThreadIds[thread], rawDataAdapter->getThreadName(thread));
    }
}
