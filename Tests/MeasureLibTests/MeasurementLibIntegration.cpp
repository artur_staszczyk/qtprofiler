﻿#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <fstream>

#define ENABLE_PROFILING
#include "PerformanceHelper.h"

using namespace PerformanceHelper;

static void runSimulation1(const std::string& fileName)
{
    volatile double result;
    volatile double test = 1000000;
    volatile double test2 = 2;

    Singleton<MeasurementBuffer>::instance().setStorage(new FileOutputStorage(fileName));

    MEASURE_SCOPE_START("divide")

    for (int i = 0; i < 100000; ++i)
        result = test / test2;

    MEASURE_SCOPE_END()

    MEASURE_SCOPE_START("multiply");

    for (int i = 0; i < 100000; ++i)
        result = test * test2;

    MEASURE_SCOPE_END()

    FRAME_END();

    MEASURE_SCOPE_START("divide")

    for (int i = 0; i < 300000; ++i)
        result = test / test2;
    MEASURE_SCOPE_END()

    MEASURE_SCOPE_START("multiply");

    for (int i = 0; i < 300000; ++i)
        result = test * test2;

    MEASURE_SCOPE_END()

    MEASURE_SCOPE_START("divide")

    for (int i = 0; i < 100000; ++i)
        result = test / test2;
    MEASURE_SCOPE_END()

    MEASURE_SCOPE_START("multiply");

    for (int i = 0; i < 100000; ++i)
        result = test * test2;

    MEASURE_SCOPE_END()

    Singleton<MeasurementBuffer>::instance().processSavedMeasurement();
    Singleton<MeasurementBuffer>::instance().save();
}

static void runSimulation2(const std::string& fileName)
{
    volatile double result;
    volatile double test = 1000000;
    volatile double test2 = 2;

    Singleton<MeasurementBuffer>::instance().setStorage(new FileOutputStorage(fileName));

    auto thread = std::thread([&](void) {
        MEASURE_SCOPE_START("divide")

        for (int i = 0; i < 1000; ++i)
            result = test / test2;

        MEASURE_SCOPE_END()

        MEASURE_SCOPE_START("multiply");

        for (int i = 0; i < 1000000; ++i)
            result = test * test2;

        MEASURE_SCOPE_END()
    });

    MEASURE_SCOPE_START("divide")

    for (int i = 0; i < 100000; ++i)
        result = test / test2;

    MEASURE_SCOPE_END()

    MEASURE_SCOPE_START("multiply");

    for (int i = 0; i < 100000; ++i)
        result = test * test2;

    MEASURE_SCOPE_END()

    FRAME_END();

    MEASURE_SCOPE_START("divide")

    for (int i = 0; i < 300000; ++i)
        result = test / test2;
    MEASURE_SCOPE_END()

    MEASURE_SCOPE_START("multiply");

    for (int i = 0; i < 300000; ++i)
        result = test * test2;

    MEASURE_SCOPE_END()

    MEASURE_SCOPE_START("divide")

    for (int i = 0; i < 100000; ++i)
        result = test / test2;
    MEASURE_SCOPE_END()

    MEASURE_SCOPE_START("multiply");

    for (int i = 0; i < 100000; ++i)
        result = test * test2;

    MEASURE_SCOPE_END()

    thread.join();

    Singleton<MeasurementBuffer>::instance().processSavedMeasurement();
    Singleton<MeasurementBuffer>::instance().save();
}

TEST(MEasureLibIntegration, TestWholeSystem)
{
    std::string fileName = "SingleThreaded.dat";
    runSimulation1(fileName);

    std::ifstream fstream(fileName, std::ios::binary);
    EXPECT_TRUE(fstream.is_open());

    auto startPos = fstream.tellg();
    fstream.seekg (0, std::ios::end);
    auto size = fstream.tellg() - startPos;
    EXPECT_EQ(size, 192);

    fstream.seekg(0, std::ios::beg);
    char* data = new char[size];
    fstream.read(data, size);

    std::size_t notCastedThreadId = std::hash<std::thread::id>()(std::this_thread::get_id());
    uint32_t thisThreadId = reinterpret_cast<uint32_t&>(notCastedThreadId);

    std::vector<uint64_t> storedData(reinterpret_cast<uint64_t*>(data), reinterpret_cast<uint64_t*>(data) + size/4);

    EXPECT_EQ(storedData[0], MeasurementBuffer::HEADER_VERSION);
    EXPECT_EQ(storedData[1], 6 * ENTRY_SIZE_IN_BYTES + FRAME_END_SIZE_IN_BYTES);

    EXPECT_LT(reinterpret_cast<double&>(storedData[2]), reinterpret_cast<double&>(storedData[3]));
    EXPECT_EQ(storedData[4], ((uint32_t)thisThreadId | ((uint64_t)0) << 32));

    EXPECT_LT(reinterpret_cast<double&>(storedData[5]), reinterpret_cast<double&>(storedData[6]));
    EXPECT_EQ(storedData[7], ((uint32_t)thisThreadId | ((uint64_t)1) << 32));

    EXPECT_EQ(storedData[8], MeasurementBuffer::FRAME_END_MARKER);

    EXPECT_LT(reinterpret_cast<double&>(storedData[9]), reinterpret_cast<double&>(storedData[10]));
    EXPECT_EQ(storedData[11], ((uint32_t)thisThreadId | ((uint64_t)0) << 32));

    EXPECT_LT(reinterpret_cast<double&>(storedData[12]), reinterpret_cast<double&>(storedData[13]));
    EXPECT_EQ(storedData[14], ((uint32_t)thisThreadId | ((uint64_t)1) << 32));

    EXPECT_LT(reinterpret_cast<double&>(storedData[15]), reinterpret_cast<double&>(storedData[16]));
    EXPECT_EQ(storedData[17], ((uint32_t)thisThreadId | ((uint64_t)0) << 32));

    EXPECT_LT(reinterpret_cast<double&>(storedData[18]), reinterpret_cast<double&>(storedData[19]));
    EXPECT_EQ(storedData[20], ((uint32_t)thisThreadId | ((uint64_t)1) << 32));

    //Funcions
    std::string expected[] = {
        "divide",
        "multiply"
    };

    EXPECT_EQ(storedData[21], sizeof(expected) / sizeof(std::string));

    const uint8_t* functionBuffer = reinterpret_cast<const uint8_t*>(&storedData[22]);
    for(int i = 0; i < 2; ++i)
    {
        uint8_t len = *functionBuffer;
        functionBuffer++;
        std::string name(reinterpret_cast<const char*>(functionBuffer),
                         reinterpret_cast<const char*>(functionBuffer) + len);
        functionBuffer += len;
        EXPECT_EQ(name, expected[i]);
    }

}

TEST(MEasureLibIntegration, TestWholeSystemMultipleThreads)
{
    std::string fileName = "MultiThreaded.dat";
    runSimulation2(fileName);

    std::ifstream fstream(fileName, std::ios::binary);
    EXPECT_TRUE(fstream.is_open());

    auto startPos = fstream.tellg();
    fstream.seekg (0, std::ios::end);
    auto size = fstream.tellg() - startPos;
    EXPECT_EQ(size, 240);

    fstream.seekg(0, std::ios::beg);
    char* data = new char[size];
    fstream.read(data, size);

    std::vector<uint64_t> storedData(reinterpret_cast<uint64_t*>(data), reinterpret_cast<uint64_t*>(data) + size/4);

    EXPECT_EQ(storedData[0], MeasurementBuffer::HEADER_VERSION);
    EXPECT_EQ(storedData[1], 8 * ENTRY_SIZE_IN_BYTES + FRAME_END_SIZE_IN_BYTES);

    //Funcions
    std::string expected[] = {
        "divide",
        "multiply"
    };

    EXPECT_EQ(storedData[27], sizeof(expected) / sizeof(std::string));

    const uint8_t* functionBuffer = reinterpret_cast<const uint8_t*>(&storedData[28]);
    for(int i = 0; i < 2; ++i)
    {
        uint8_t len = *functionBuffer;
        functionBuffer++;
        std::string name(reinterpret_cast<const char*>(functionBuffer),
                         reinterpret_cast<const char*>(functionBuffer) + len);
        functionBuffer += len;
        EXPECT_EQ(name, expected[i]);
    }

}
