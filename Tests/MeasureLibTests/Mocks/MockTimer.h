﻿#ifndef MOCKTIMER_H
#define MOCKTIMER_H

#include <HighPerfTimer.h>

class MockTimer
{
public:

    MOCK_CONST_METHOD0(millisecondsElapsed, double());
};


#endif // MOCKTIMER_H
