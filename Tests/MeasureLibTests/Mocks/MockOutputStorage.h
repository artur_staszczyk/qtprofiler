﻿#ifndef MOCKFILESTREAM_H
#define MOCKFILESTREAM_H

#include <gmock/gmock.h>

#include <Storage/OutputStorage.h>

class MockOutputStorage : public PerformanceHelper::OutputStorage
{
public:
    MOCK_METHOD2(store, void(const uint8_t*, uint32_t));
    MOCK_METHOD2(load, void(uint8_t**, uint32_t&));

    void delegateToFake()
    {
        ON_CALL(*this, store(::testing::_, ::testing::_)).
                WillByDefault(::testing::Invoke(this, &MockOutputStorage::fakeStore));
    }

    std::vector<uint64_t> StoredData;

private:
    void fakeStore(const uint8_t* data, uint32_t size)
    {
        auto sizeInFullUint64 = (size / sizeof(uint64_t));
        auto tail = size % sizeof(uint64_t) > 0 ? 1 : 0;

        std::vector<uint64_t> newVec(reinterpret_cast<const uint64_t*>(data),
                                    reinterpret_cast<const uint64_t*>(data) + sizeInFullUint64 + tail);
        StoredData.swap(newVec);
    }

};

#endif // MOCKFILESTREAM_H
