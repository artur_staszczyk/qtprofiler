﻿#ifndef MOCKMEASUREMENTBUFFER
#define MOCKMEASUREMENTBUFFER

#include <gmock/gmock.h>

#include "Measurement.h"
#include "MeasurementBuffer.h"

#include "MockTimer.h"

using namespace PerformanceHelper;

class MockMeasurementBuffer
{
public:

    MOCK_METHOD1(queueMeasurement, void(const MeasurementData&));

    void delegateToFake()
    {
        ON_CALL(*this, queueMeasurement(::testing::_)).WillByDefault(::testing::Invoke(this, &MockMeasurementBuffer::fakeSaveMeasurement));
    }

public:
    MeasurementData savedData;

protected:
    void fakeSaveMeasurement(const MeasurementData& data)
    {
        savedData = data;
    }
};

#endif
