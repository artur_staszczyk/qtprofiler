#-------------------------------------------------
#
# Project created by QtCreator 2015-10-03T16:18:34
#
#-------------------------------------------------
include(../../include.pri)

TEMPLATE = app
TARGET = MeasureLibTests

CONFIG   += console
CONFIG   -= app_bundle

QT += core testlib widgets
QT -= gui

INCLUDEPATH += $$PWD/../../include/MeasureLib

win32:CONFIG(release, debug|release): {
    #TODO get from libs-qt
    PRE_TARGETDEPS += $$OUT_PWD/../../QtMeasureLib/release/QtMeasureLib.lib
    #TODO LIBS
}
else:win32:CONFIG(debug, debug|release): {
    #TODO get from libs-qt
    PRE_TARGETDEPS += $$OUT_PWD/../../QtMeasureLib/debug/QtMeasureLib.lib
    #TODO LIBS
}
else:unix:CONFIG(release, debug|release): {
    PRE_TARGETDEPS += $$PWD/../../libs-qt/libmeasureLib.a
    LIBS += -L$$PWD/../../libs-qt -lmeasureLib
}
else:unix:CONFIG(debug, debug|release): {
    PRE_TARGETDEPS += $$PWD/../../libs-qt/libMeasureLibd.a
    LIBS += -L$$PWD/../../libs-qt -lmeasureLibd
}

SOURCES  += main.cpp \
    MeasureLibTests.cpp \
    MeasurementLibIntegration.cpp

HEADERS += \
    Mocks/MockTimer.h \
    Mocks/MockMeasurementBuffer.h \
    Mocks/MockOutputStorage.h
