﻿#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <memory>
#include <PerformanceHelper.h>
#include <Storage/NullOutputStorage.h>

#include "Mocks/MockTimer.h"
#include "Mocks/MockOutputStorage.h"
#include "Mocks/MockMeasurementBuffer.h"

using namespace PerformanceHelper;
using namespace ::testing;
using ::testing::Return;
using ::testing::Invoke;

using MeasurementForTest = Measurement<MockMeasurementBuffer, MockTimer>;

static MeasurementData testMeasurement(const char* name)
{
    MeasurementData data;
    data.Name = name;
    data.StartTime = 1;
    data.EndTime = 2;
    data.ThreadId = 12;

    return data;
}

TEST(MeasureLibV2, TestSingleMeasurement)
{
    MockMeasurementBuffer buffer;
    buffer.delegateToFake();

    MockTimer timer;

    EXPECT_CALL(timer, millisecondsElapsed()).Times(2).WillOnce(Return(1)).WillOnce(Return(2));
    EXPECT_CALL(buffer, queueMeasurement(_));

    std::auto_ptr<MeasurementForTest> measurement(new MeasurementForTest("TEST", buffer, timer));
    measurement.reset();

    EXPECT_EQ(buffer.savedData.StartTime, 1);
    EXPECT_EQ(buffer.savedData.EndTime, 2);
    EXPECT_EQ(buffer.savedData.Name, "TEST");
    EXPECT_NE(buffer.savedData.ThreadId, 0);
}


TEST(MeasureLibV2, TestMeasurementBufferSave)
{
    MeasurementBuffer buffer;
    auto storage = new MockOutputStorage();
    buffer.setStorage(storage);

    EXPECT_CALL(*storage, store(_, _)).Times(2);

    buffer.save();
}

TEST(MeasureLibV2, TestMeasurementBufferSaveFrame)
{
    MeasurementBuffer buffer;

    buffer.queueFrameEnd();
    buffer.processSavedMeasurement();

    auto endSize = buffer.sizeInBytesOfMeasuredData();
    auto measurementBuffer = buffer.getMeasurementBuffer();

    EXPECT_EQ(endSize, sizeof(int64_t));
    EXPECT_EQ(measurementBuffer[0], 0xFF);
    EXPECT_EQ(measurementBuffer[1], 0xFF);
    EXPECT_EQ(measurementBuffer[2], 0xFF);
    EXPECT_EQ(measurementBuffer[3], 0xFF);

    EXPECT_EQ(measurementBuffer[4], 0x00);
    EXPECT_EQ(measurementBuffer[5], 0x00);
    EXPECT_EQ(measurementBuffer[6], 0x00);
    EXPECT_EQ(measurementBuffer[7], 0x00);

}

TEST(MeasureLibV2, TestMeasurementReset)
{
    MeasurementBuffer buffer;
    auto data = testMeasurement("TEST");

    buffer.queueMeasurement(data);
    buffer.processSavedMeasurement();

    EXPECT_EQ (buffer.sizeInBytesOfMeasuredData(), ENTRY_SIZE_IN_BYTES);
    EXPECT_NE(*reinterpret_cast<const uint64_t*>(buffer.getMeasurementBuffer()), MeasurementBuffer::HEADER_VERSION);

    buffer.setStorage(new NullOutputStorage());
    EXPECT_EQ(buffer.sizeInBytesOfMeasuredData(), 0);
    EXPECT_EQ(*reinterpret_cast<const uint64_t*>(buffer.getMeasurementBuffer()), 0L);
}

TEST(MeasureLibV2, TestMeasurementBufferSaveMeasure)
{
    MeasurementBuffer buffer;

    // given
    auto data = testMeasurement("TEST");
    buffer.queueMeasurement(data);
    buffer.processSavedMeasurement();

    EXPECT_EQ (buffer.sizeInBytesOfMeasuredData(), ENTRY_SIZE_IN_BYTES);

    data = testMeasurement("TEST2");
    buffer.queueMeasurement(data);
    buffer.processSavedMeasurement();
    EXPECT_EQ (buffer.sizeInBytesOfMeasuredData(), 2 * ENTRY_SIZE_IN_BYTES);

    auto measuremenBuffer = buffer.getMeasurementBuffer();

    // expect
    // First measurement
    EXPECT_EQ (reinterpret_cast<const double&>(measuremenBuffer[0]), data.StartTime);
    EXPECT_EQ (reinterpret_cast<const double&>(measuremenBuffer[8]), data.EndTime);
    EXPECT_EQ (reinterpret_cast<const uint64_t&>(measuremenBuffer[16]),
            ((uint64_t)data.ThreadId | ((uint64_t)0 << 32)));

    // Second measurement

    EXPECT_EQ (reinterpret_cast<const double&>(measuremenBuffer[24]), data.StartTime);
    EXPECT_EQ (reinterpret_cast<const double&>(measuremenBuffer[32]), data.EndTime);
    EXPECT_EQ (reinterpret_cast<const uint64_t&>(measuremenBuffer[40]),
            ((uint64_t)data.ThreadId | ((uint64_t)1 << 32)));

    // Funciton names
    auto functionBuffer = buffer.getFunctionBuffer();
    EXPECT_EQ(std::string(functionBuffer[0]), "TEST");
    EXPECT_EQ(std::string(functionBuffer[1]), "TEST2");
}

TEST (MeasureLibV2, TestMeasurementBufferNotBigEnough)
{
    MeasurementBuffer buffer;

    buffer.setBufferSize(ENTRY_SIZE_IN_BYTES);

    auto data = testMeasurement("TEST");
    buffer.queueMeasurement(data);
    data = testMeasurement("TEST2");
    buffer.queueMeasurement(data);

    buffer.processSavedMeasurement();
    auto measurementBuffer = buffer.getMeasurementBuffer();

    EXPECT_EQ (buffer.sizeInBytesOfMeasuredData(), ENTRY_SIZE_IN_BYTES);

    EXPECT_EQ (reinterpret_cast<const double&>(measurementBuffer[0]), data.StartTime);
    EXPECT_EQ (reinterpret_cast<const double&>(measurementBuffer[8]), data.EndTime);

    EXPECT_EQ (reinterpret_cast<const uint64_t&>(measurementBuffer[16]),
            ((uint64_t)data.ThreadId | ((uint64_t)0 << 32)));


    auto functionBuffer = buffer.getFunctionBuffer();
    EXPECT_EQ(std::string(functionBuffer[0]), "TEST");
}

TEST (MeasureLibV2, TestMeasurementFunctionBufferNotBigEnough)
{
    MeasurementBuffer buffer;

    buffer.setMaxFunctionNames(1);

    auto data = testMeasurement("TEST");
    buffer.queueMeasurement(data);
    data = testMeasurement("TEST2");
    buffer.queueMeasurement(data);

    buffer.processSavedMeasurement();
    auto measurementBuffer = buffer.getMeasurementBuffer();

    EXPECT_EQ (buffer.sizeInBytesOfMeasuredData(), ENTRY_SIZE_IN_BYTES);

    EXPECT_EQ (reinterpret_cast<const double&>(measurementBuffer[0]), data.StartTime);
    EXPECT_EQ (reinterpret_cast<const double&>(measurementBuffer[8]), data.EndTime);

    EXPECT_EQ (reinterpret_cast<const uint64_t&>(measurementBuffer[16]),
            ((uint32_t)data.ThreadId | ((uint64_t)0) << 32));

    auto functionBuffer = buffer.getFunctionBuffer();
    EXPECT_EQ(std::string(functionBuffer[0]), "TEST");
}

TEST (MeasureLibV2, TestDataConsistency)
{
    MeasurementBuffer buffer;

    buffer.setMaxFunctionNames(1);

    auto data = testMeasurement("TEST");
    buffer.queueMeasurement(data);
    data = testMeasurement("TEST2");
    buffer.queueMeasurement(data);

    buffer.processSavedMeasurement();
    auto measurementBuffer = buffer.getMeasurementBuffer();

    EXPECT_EQ (buffer.sizeInBytesOfMeasuredData(), ENTRY_SIZE_IN_BYTES);

    EXPECT_EQ (reinterpret_cast<const double&>(measurementBuffer[0]), data.StartTime);
    EXPECT_EQ (reinterpret_cast<const double&>(measurementBuffer[8]), data.EndTime);

    EXPECT_EQ (reinterpret_cast<const uint64_t&>(measurementBuffer[16]),
            ((uint32_t)data.ThreadId | ((uint64_t)0) << 32));

    auto functionBuffer = buffer.getFunctionBuffer();
    EXPECT_EQ(std::string(functionBuffer[0]), "TEST");
}

TEST(MeasureLibV2, TestStorageBufferLayout)
{
    auto storage = new MockOutputStorage();
    MeasurementBuffer buffer;

    storage->delegateToFake();
    EXPECT_CALL(*storage, store(_, _)).Times(2);
    buffer.setStorage(storage);

    auto data = testMeasurement("TEST");
    buffer.queueMeasurement(data);
    buffer.queueFrameEnd();

    buffer.processSavedMeasurement();
    buffer.save();

    // assert
    auto storedData = storage->StoredData;
    EXPECT_EQ(storedData[0], MeasurementBuffer::HEADER_VERSION);
    EXPECT_EQ(storedData[1], ENTRY_SIZE_IN_BYTES + FRAME_END_SIZE_IN_BYTES);
    EXPECT_DOUBLE_EQ(reinterpret_cast<double&>(storedData[2]), data.StartTime);
    EXPECT_DOUBLE_EQ(reinterpret_cast<double&>(storedData[3]), data.EndTime);
    EXPECT_EQ(storedData[4], ((uint32_t)data.ThreadId | ((uint64_t)0) << 32));
    EXPECT_EQ(storedData[5], MeasurementBuffer::FRAME_END_MARKER);
    EXPECT_EQ(storedData[6], 1); // measurement function count

    const uint8_t* functionNames = reinterpret_cast<const uint8_t*>(&storedData[7]);
    EXPECT_EQ(functionNames[0], strlen(data.Name));
    std::string functionName(reinterpret_cast<const char*>(&functionNames[1]), strlen(data.Name));
    EXPECT_EQ(functionName, data.Name);
}
