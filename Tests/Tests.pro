TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS += \
    ProfilerLibIntegration \
    MeasureLibTests \
    ProfilerLibTests \
